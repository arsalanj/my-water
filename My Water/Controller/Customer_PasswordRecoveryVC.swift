//
//  Customer_PasswordRecoveryVC.swift
//  My Water
//
//  Created by Muhammad Mehdi on 07/05/2019.
//  Copyright © 2019 Muhammad Mehdi. All rights reserved.
//

import UIKit
import MaterialComponents

class Customer_PasswordRecoveryVC: BaseViewController {

    
    
//    @IBOutlet var signInLabel: UILabel!
    @IBOutlet var topLabel: UILabel!
    
    
    @IBOutlet var phoneNumberLabel: UILabel!
    @IBOutlet var CountryCodeTextField: MDCTextField!
    @IBOutlet var mobileNumberTextField: MDCTextField!
    @IBOutlet var phoneNumberErrorLbl: UILabel!

    @IBOutlet var signInBtn: UIButton!

    //textfield controller
    
    var countryCodeController: MDCTextInputControllerOutlined?
    var mobileNumberController: MDCTextInputControllerOutlined?
    
    var textFieldArray : [MDCTextField]?
    var textFieldControllerArray : [MDCTextInputControllerOutlined]?
    var textFieldErrorLblArray : [UILabel]?
    var placeHolderArray = ["00 0000000","2"]
    var isValidated : Bool = false
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setUpTextFieldControllers()
        setUIStyle()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        
        navigationController?.setNavigationBarHidden(false, animated: animated)
        super.showLeftLabel(show: true, heading: "Password Recovery")

    }
    
    func setUpTextFieldControllers(){
        
        
        
        countryCodeController = MDCTextInputControllerOutlined(textInput: CountryCodeTextField)
        mobileNumberController = MDCTextInputControllerOutlined(textInput: mobileNumberTextField)
 
        mobileNumberController?.placeholderText = placeHolderArray.first
        //        mobileNumberController?.isFloatingEnabled = false
        
        
        //        mobileNumberController?.characterCountMax = 10
        //        mobileNumberController?.isFloatingEnabled = false
        
        
        
        
        CountryCodeTextField.delegate = self
        mobileNumberTextField.delegate = self
 
        textFieldArray = [CountryCodeTextField,mobileNumberTextField]
        textFieldControllerArray = [countryCodeController,mobileNumberController] as! [MDCTextInputControllerOutlined]
        textFieldErrorLblArray = [UILabel(), phoneNumberErrorLbl] as! [UILabel]
        
        
        
        
        
        
    }
    
    func setUIStyle(){
        
        //textfield
        
        
        
        for controller in textFieldControllerArray!{
            
            //            controller.underlineViewMode = .never
            //             controller.borderFillColor = UIColor.white
            controller.textInputFont = ThemeEngine.sharedInstance.getFontNormal(size: 14)
            controller.inlinePlaceholderFont = ThemeEngine.sharedInstance.getFontNormal(size: 14)
            
        }
        countryCodeController?.borderFillColor = UIColor.whiteTwo
        
        //error labels
        
        for labels in textFieldErrorLblArray!{
            labels.attributedText = ThemeEngine.sharedInstance.attributedString(alignment: .left, font: ThemeEngine.sharedInstance.getFontNormal(size: 12), color: UIColor.red, text: "")
            labels.numberOfLines = 2
            labels.lineBreakMode = .byWordWrapping
            labels.adjustsFontSizeToFitWidth = true
            
            labels.isHidden = true
        }
        
        
        
        //sign in labels
        topLabel.attributedText = ThemeEngine.sharedInstance.attributedString(alignment: .center, font: ThemeEngine.sharedInstance.getFontNormal(size: 12), color: UIColor.brownGreyTwo, text: "Please enter your registered email address or phone number we will send you link instructions for your registered type")
        
        phoneNumberLabel.attributedText = ThemeEngine.sharedInstance.attributedString(alignment: .left, font: ThemeEngine.sharedInstance.getFontNormal(size: 13), color: UIColor.black, text: "Phone Number")
        
        
        
        
        signInBtn.setAttributedTitle(ThemeEngine.sharedInstance.attributedString(alignment: .center, font: ThemeEngine.sharedInstance.getFontMedium(size: 15), color: UIColor.white, text: "Send Reset Instructions"), for: .normal)
        signInBtn.setBackgroundColor(color: UIColor.primaryBlue, forState: .normal)
        signInBtn.layer.cornerRadius = 6
        signInBtn.clipsToBounds = true
        
        
        
        
        
    }
    
    
    
    @IBAction func buttonPressed(_ sender: UIButton) {
        switch sender {

        
        case signInBtn:
        //            passwordController?.setErrorText("Oops, we don't have an account with this phone. Would you like to register an account?", errorAccessibilityValue: nil)
//            UIUtility.sharedInstance.setError(error: true, message: "Oops, we don't have this email in our database. Please check it   again or contact our support for help.", textFieldController: mobileNumberController!,textFieldErrorLblArray: textFieldErrorLblArray!, textFieldControllerArray: textFieldControllerArray!)
            self.navigationController?.pushViewController(UIUtility.sharedInstance.instantiateVC(storyboard: "Customer_MyDevices", identifier: "Customer_MyDevicesVC"), animated: true)
//            self.present(, animated: true, completion: nil)
            
            
        default:
            print()
        }
        
    }

}




//MARK:- Textfield delegates

extension Customer_PasswordRecoveryVC: UITextFieldDelegate{


    
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        
        
        for (index,controller) in (textFieldControllerArray?.enumerated())!{
            
            if textField == textFieldArray![index] {
                
                controller.placeholderText = ""
                
            }
            
            
            
        }
        
        
        
    }
    
    
    
    func textFieldDidEndEditing(_ textField: UITextField, reason: UITextField.DidEndEditingReason) {
        
        
        
        
        for (index,controller) in (textFieldControllerArray?.enumerated())!{
            
            if textField == textFieldArray![index] {
                
                
                if textField.text!.isEmpty {
                    controller.placeholderText = placeHolderArray[index]
                    
                }else{
                    controller.placeholderText = ""
                }
            }
            
            
            
        }
    }
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        for (index,controller) in (textFieldControllerArray?.enumerated())!{
            if textField == textFieldArray![index] {
                
                UIUtility.sharedInstance.setError(error: false, message: "", textFieldController: controller,textFieldErrorLblArray: textFieldErrorLblArray!,textFieldControllerArray: textFieldControllerArray!)
            }        }
        
        
        return true
    }
    

}
