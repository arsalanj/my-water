//
//  Customer_BillPayVC.swift
//  My Water
//
//  Created by Muhammad Mehdi on 13/05/2019.
//  Copyright © 2019 Muhammad Mehdi. All rights reserved.
//

import UIKit
import MaterialComponents
import SnapKit


class Customer_BillPayVC: BaseViewController {
    
    
    @IBOutlet var invoiceHeadingLabel: UILabel!
    @IBOutlet var invoiceValueLabel: UILabel!

    @IBOutlet var dateCreatedHeadingLabel: UILabel!
    @IBOutlet var dateCreatedValueLabel: UILabel!

    @IBOutlet var modelIDHeadingLabel: UILabel!
    @IBOutlet var modelIDValueLabel: UILabel!

    @IBOutlet var bundleTypeHeadingLabel: UILabel!
    @IBOutlet var bundleTypeValueLabel: UILabel!

    @IBOutlet var amountHeadingLabel: UILabel!
    @IBOutlet var amountValueLabel: UILabel!

    
    @IBOutlet var paymentMethodLabel: UILabel!
    
    @IBOutlet var paymentCollectionView: UICollectionView!
    @IBOutlet var creditCardStackView: UIStackView!
    //credit card
    
    @IBOutlet var cardFieldLabel: UILabel!
    @IBOutlet var cardTextField: MDCTextField!
    @IBOutlet var cardErrorLabel: UILabel!
    
    @IBOutlet var nameFieldLabel: UILabel!
    @IBOutlet var nameTextField: MDCTextField!
    @IBOutlet var nameErrorLabel: UILabel!
    
    @IBOutlet var expiryDateFieldLabel: UILabel!
    @IBOutlet var expiryDateTextField: MDCTextField!
    @IBOutlet var expiryDateErrorLabel: UILabel!
    
    @IBOutlet var cvvFieldLabel: UILabel!
    @IBOutlet var cvvTextField: MDCTextField!
    @IBOutlet var cvvErrorLabel: UILabel!
    @IBOutlet var cvvHelpButton: UIButton!
    
    
    @IBOutlet var payNowButton: UIButton!
    
    var headingLabelArray : [UILabel]?
    var valueLabelArray : [UILabel]?
    var headingString = ["Invoice#:","Date Created:","Model ID#:","Bundle Type:","Amount:"]
    var valueString = ["2020","06/8/2019","2320932093sdjsd23J","[Bundle Type]","1800 PKR"]
    
    
    var paymentOptionImages : [UIImage] = [UIImage(),#imageLiteral(resourceName: "jazzLogo2"),#imageLiteral(resourceName: "websiteSizeLogo")]
    
    var selectedIndexPath: IndexPath = IndexPath(row: 0, section: 0) {
        didSet{
            paymentCollectionView.reloadData()
        }
    }
    
    //textfield controller
    
    var cardController: MDCTextInputControllerOutlined?
    var nameController: MDCTextInputControllerOutlined?
    var expiryDateController: MDCTextInputControllerOutlined?
    var cvvController: MDCTextInputControllerOutlined?
    
    
    var textFieldArray : [MDCTextField]?
    var textFieldControllerArray : [MDCTextInputControllerOutlined]?
    var textFieldErrorLblArray : [UILabel]?
    var textFieldHeadingLblArray : [UILabel]?
    var placeHolderArray = ["0000 0000 0000 0000","","00/00",""]
    
    
    
    var isBillPaid = false
    
    override func viewDidLoad() {
        super.viewDidLoad()

//        super.addSideMenu()
        
        if !isBillPaid{
        super.showLeftLabel(show: true, heading: "Pay My Bill")
        }else{
            super.showLeftLabel(show: true, heading: "My Paid Bill")

        }
            super.showLeftMenu(show: false)

        
        headingLabelArray = [invoiceHeadingLabel,dateCreatedHeadingLabel,modelIDHeadingLabel,bundleTypeHeadingLabel,amountHeadingLabel]
        valueLabelArray = [invoiceValueLabel,dateCreatedValueLabel,modelIDValueLabel,bundleTypeValueLabel,amountValueLabel]
        
      
        
        paymentCollectionView.delegate = self
        paymentCollectionView.dataSource = self
        
        paymentCollectionView.reloadData()
        
        
        setUpTextFieldControllers()
      
        setUIStyle()

        
        if isBillPaid {
            
            paymentMethodLabel.isHidden = true
            paymentCollectionView.isHidden = true
            creditCardStackView.isHidden = true
            payNowButton.isHidden = true
            
        }
    
    }
    func setUpTextFieldControllers(){
        
        cardController = MDCTextInputControllerOutlined(textInput: cardTextField)
        nameController = MDCTextInputControllerOutlined(textInput: nameTextField)
        expiryDateController = MDCTextInputControllerOutlined(textInput: expiryDateTextField)
        cvvController = MDCTextInputControllerOutlined(textInput: cvvTextField)

        
        
        textFieldArray = [cardTextField, nameTextField,expiryDateTextField,cvvTextField]
        textFieldControllerArray = [cardController,nameController,expiryDateController,cvvController] as! [MDCTextInputControllerOutlined]
        textFieldErrorLblArray = [cardErrorLabel,nameErrorLabel,expiryDateErrorLabel,cvvErrorLabel]
        
        
        
        
        for textField in textFieldArray! {
            textField.delegate = self
        }
    }
    
    func setUIStyle(){
        
        
        //textfield
        
        for (index,controller) in textFieldControllerArray!.enumerated(){
            
            //            controller.underlineViewMode = .never
            //             controller.borderFillColor = UIColor.white
            controller.textInputFont = ThemeEngine.sharedInstance.getFontNormal(size: 14)
            controller.inlinePlaceholderFont = ThemeEngine.sharedInstance.getFontNormal(size: 14)
            controller.placeholderText = placeHolderArray[index]
            
        }
 
        //error labels
        
        for labels in textFieldErrorLblArray!{
            labels.attributedText = ThemeEngine.sharedInstance.attributedString(alignment: .left, font: ThemeEngine.sharedInstance.getFontNormal(size: 12), color: UIColor.red, text: "")
            labels.numberOfLines = 2
            labels.lineBreakMode = .byWordWrapping
            labels.adjustsFontSizeToFitWidth = true
            
            labels.isHidden = true
        }
        
       
        
        //textfield labels
                cardFieldLabel.attributedText = ThemeEngine.sharedInstance.attributedString(alignment: .left, font: ThemeEngine.sharedInstance.getFontMedium(size: 13), color: UIColor.black, text: "Card Number")
                nameFieldLabel.attributedText = ThemeEngine.sharedInstance.attributedString(alignment: .left, font: ThemeEngine.sharedInstance.getFontMedium(size: 13), color: UIColor.black, text: "Cardholder Name")
                expiryDateFieldLabel.attributedText = ThemeEngine.sharedInstance.attributedString(alignment: .left, font: ThemeEngine.sharedInstance.getFontMedium(size: 13), color: UIColor.black, text: "Expiry Date")
                cvvFieldLabel.attributedText = ThemeEngine.sharedInstance.attributedString(alignment: .left, font: ThemeEngine.sharedInstance.getFontMedium(size: 13), color: UIColor.black, text: "CVV")
        
        
        
        //top labels
        for (index,label) in headingLabelArray!.enumerated() {
            
            label.attributedText = ThemeEngine.sharedInstance.attributedString(alignment: .left, font: ThemeEngine.sharedInstance.getFontMedium(size: 13), color: UIColor.blackTwoTwo, text: headingString[index])
            label.adjustsFontSizeToFitWidth = true
            
            
        }
        
        
        for (index,label) in valueLabelArray!.enumerated() {
            
            label.attributedText = ThemeEngine.sharedInstance.attributedString(alignment: .left, font: ThemeEngine.sharedInstance.getFontNormal(size: 13), color: UIColor.black, text: valueString[index])
            label.adjustsFontSizeToFitWidth = true
            
            
        }
        
        
        paymentMethodLabel.attributedText = ThemeEngine.sharedInstance.attributedString(alignment: .left, font: ThemeEngine.sharedInstance.getFontMedium(size: 13), color: UIColor.blackTwoTwo, text: "Payment Method")
        paymentMethodLabel.adjustsFontSizeToFitWidth = true
        
        payNowButton.setAttributedTitle(ThemeEngine.sharedInstance.attributedString(alignment: .center, font: ThemeEngine.sharedInstance.getFontMedium(size: 15), color: UIColor.white, text: "Pay Now"), for: .normal)
        payNowButton.setBackgroundColor(color: UIColor.primaryBlue, forState: .normal)
        payNowButton.layer.cornerRadius = 6
        payNowButton.clipsToBounds = true
        

        
    }

    
    func showHideCardView(hide : Bool) {
        
        creditCardStackView.isHidden = hide
        
 
        
    }
    
    
    @IBAction func payNowButtonPresesd(_ sender: UIButton) {
    }
    
    
    @IBAction func cvvHelpButtonPressed(_ sender: UIButton) {
        
    }
    
    
    
    
}



//MARK:- Textfield delegates

extension Customer_BillPayVC: UITextFieldDelegate{
    
    
    
    
    
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        
        
        for (index,controller) in (textFieldControllerArray?.enumerated())!{
            
            if textField == textFieldArray![index] {
                
                controller.placeholderText = ""
                
            }
            
            
            
        }
        
        
        
    }
    
    
    
    func textFieldDidEndEditing(_ textField: UITextField, reason: UITextField.DidEndEditingReason) {
        
        
        
        
        for (index,controller) in (textFieldControllerArray?.enumerated())!{
            
            if textField == textFieldArray![index] {
                
                
                if textField.text!.isEmpty {
                    controller.placeholderText = placeHolderArray[index]
                    
                }else{
                    controller.placeholderText = ""
                }
            }
            
            
            
        }
    }
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        for (index,controller) in (textFieldControllerArray?.enumerated())!{
            if textField == textFieldArray![index] {
                
                UIUtility.sharedInstance.setError(error: false, message: "", textFieldController: controller,textFieldErrorLblArray: textFieldErrorLblArray!,textFieldControllerArray: textFieldControllerArray!)
            }        }
        
        
        return true
    }
    
    
}


//MARK:- collectionView
extension Customer_BillPayVC: UICollectionViewDataSource, UICollectionViewDelegate{
    
   
   
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        print("---> function = \(#function) --> Line = \(#line) ")
        return paymentOptionImages.count
    }
    
  
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        print("---> function = \(#function) --> Line = \(#line) ")

        
        if let cell = paymentCollectionView.dequeueReusableCell(withReuseIdentifier: "Customer_BillPayCVCell", for: indexPath) as? Customer_BillPayCVCell {
            //            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Customer_BillPayCVCell", for: indexPath) as! Customer_BillPayCVCell
            
            
                        var borderColor: CGColor! = UIColor.clear.cgColor
                        var borderWidth: CGFloat = 0
            
                        if indexPath == selectedIndexPath as IndexPath{
                            borderColor = UIColor.primaryBlue.cgColor
                            borderWidth = 1 //or whatever you please
                            cell.mainView.layer.cornerRadius = 6
                            
                            
                            
                        }else{
                            borderColor = UIColor.clear.cgColor
                            borderWidth = 0
                            cell.mainView.layer.borderWidth = 1
                            cell.mainView.layer.borderColor = UIColor.veryLightPinkFive.cgColor
                            cell.mainView.layer.cornerRadius = 6
                            cell.mainView.layer.shadowColor = UIColor.black2.cgColor
                            cell.mainView.layer.shadowOpacity = 1
                            cell.mainView.layer.shadowOffset = CGSize(width: 0, height: 6)
                            cell.mainView.layer.shadowRadius = 10 / 2
                            cell.mainView.layer.shadowPath = nil
                        }
            
                        cell.layer.borderWidth = borderWidth //You can use your component
                        cell.layer.borderColor = borderColor
            
            
            cell.imageView.image = paymentOptionImages[indexPath.row]
            
            
            return cell
        }
        
        return Customer_BillPayCVCell()
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
                        selectedIndexPath = indexPath
        
        if indexPath.row != 0 {
            showHideCardView(hide: true)
        }else{
            showHideCardView(hide: false)

        }
        

    }
    
    
    
 
    
    
    
    
}
