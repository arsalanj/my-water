//
//  Customer_PayMyBillVC.swift
//  My Water
//
//  Created by Muhammad Mehdi on 13/05/2019.
//  Copyright © 2019 Muhammad Mehdi. All rights reserved.
//

import UIKit

class Customer_PayMyBillVC: BaseViewController {

    @IBOutlet var noDevicesStackView: UIStackView!
    @IBOutlet var noDevicesLabel: UILabel!
    @IBOutlet var contactSalesButton: UIButton!
    
    
    @IBOutlet var listOfInvoicesLabel: UILabel!
    
    @IBOutlet var dateCreatedLabel: UILabel!
    @IBOutlet var bundleTypeLabel: UILabel!
    @IBOutlet var amountLabel: UILabel!

    @IBOutlet var statusButton: UIButton!
    
    @IBOutlet var tableView: UITableView!
    @IBOutlet var showMoreButton: UIButton!
    
    var sideMenuOptions_label = ["Scan for quality","My devices","Reports","Pay my bill","","","",""]

    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        super.addSideMenu()
        super.showLeftLabel(show: true, heading: "Pay My Bill")
        super.showLeftMenu(show: true)
        super.setNavBarButtons(selfObj: self)
        
        
        setUIStyle()
        
        tableView.delegate = self
        tableView.dataSource = self
        
        //if no device show no devices stack view

        
        noDevicesStackView.isHidden = true


    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationItem.setHidesBackButton(true, animated:true);
    }
 
    func setUIStyle(){

        
        //noDevicesLabel label
        noDevicesLabel.attributedText = ThemeEngine.sharedInstance.attributedString(alignment: .center, font: ThemeEngine.sharedInstance.getFontMedium(size: 13), color: UIColor.blackTwoTwo, text: "You don’t have any device yet?")
        noDevicesLabel.adjustsFontSizeToFitWidth = true
        
        //contact sales button
        contactSalesButton.setAttributedTitle(ThemeEngine.sharedInstance.attributedString(alignment: .center, font: ThemeEngine.sharedInstance.getFontMedium(size: 15), color: UIColor.white, text: "Contact Sales"), for: .normal)
        contactSalesButton.setBackgroundColor(color: UIColor.primaryBlue, forState: .normal)
        contactSalesButton.layer.cornerRadius = 6
        contactSalesButton.clipsToBounds = true
        
        
        listOfInvoicesLabel.attributedText = ThemeEngine.sharedInstance.attributedString(alignment: .center, font: ThemeEngine.sharedInstance.getFontMedium(size: 14), color: UIColor.black, text: "List of Invoices")
        listOfInvoicesLabel.adjustsFontSizeToFitWidth = true
        
        dateCreatedLabel.attributedText = ThemeEngine.sharedInstance.attributedString(alignment: .center, font: ThemeEngine.sharedInstance.getFontMedium(size: 11), color: UIColor.blackTwoTwo, text: "Date created")
        dateCreatedLabel.adjustsFontSizeToFitWidth = true
        
        bundleTypeLabel.attributedText = ThemeEngine.sharedInstance.attributedString(alignment: .center, font: ThemeEngine.sharedInstance.getFontMedium(size: 11), color: UIColor.blackTwoTwo, text: "Bundle Type")
        bundleTypeLabel.adjustsFontSizeToFitWidth = true
        
        amountLabel.attributedText = ThemeEngine.sharedInstance.attributedString(alignment: .center, font: ThemeEngine.sharedInstance.getFontMedium(size: 11), color: UIColor.blackTwoTwo, text: "Amount")
        amountLabel.adjustsFontSizeToFitWidth = true
        
 
        statusButton.setAttributedTitle(ThemeEngine.sharedInstance.attributedString(alignment: .center, font: ThemeEngine.sharedInstance.getFontMedium(size: 11), color: UIColor.blackTwoTwo, text: "All Statuses"), for: .normal)
        statusButton.rightImage(image: UIImage(named: "ic_triangle")!, renderMode: .alwaysTemplate)
        
        
       
        let textForButton = NSMutableAttributedString()
        let temp = ThemeEngine.sharedInstance.attributedString(alignment: .center, font: ThemeEngine.sharedInstance.getFontNormal(size: 12), color: UIColor.brownGreyTwo, text: "Show more")
        textForButton.append(temp)
        textForButton.patternDash(term: "Show more")
        
        showMoreButton.setAttributedTitle(textForButton, for: .normal)
 
        
        
    }
    
    @IBAction func contactSalesBtnPressed(_ sender: UIButton) {
        
        
    }
    

}
//MARK:- tableview delegates
extension Customer_PayMyBillVC: UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return sideMenuOptions_label.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "Customer_PayMyBillTVCell", for: indexPath) as? Customer_PayMyBillTVCell {
            
            cell.mapDataOnCell(cell: cell, indexPath: indexPath, array: sideMenuOptions_label)
            
            cell.selectionStyle = .none
            return cell
        }
        
        return Customer_PayMyBillTVCell()
    }
    
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        
        self.navigationController?.pushViewController(UIUtility.sharedInstance.instantiateVC(storyboard: "Customer_BillPay", identifier: "Customer_BillPayVC"), animated: true)
        
    }
}
