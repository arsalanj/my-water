//
//  CustomerGuestQualityVC.swift
//  My Water
//
//  Created by Muhammad Mehdi on 02/05/2019.
//  Copyright © 2019 Muhammad Mehdi. All rights reserved.
//

import UIKit

class CustomerGuestQualityVC: UIViewController {
    
    
    @IBOutlet var label_1_Drop: UILabel!
    @IBOutlet var label_2_Drop: UILabel!

    
    @IBOutlet var todayButton: UIButton!
    @IBOutlet var weekButton: UIButton!
    @IBOutlet var monthButton: UIButton!
    @IBOutlet var yearButton: UIButton!
    
    
    @IBOutlet var label_1_Tap: UILabel!
    @IBOutlet var label_2_Tap: UILabel!

    @IBOutlet var label_1_Glass: UILabel!
    @IBOutlet var label_2_Glass: UILabel!

    @IBOutlet var label_1_Bottle: UILabel!
    @IBOutlet var label_2_Bottle: UILabel!

    
    
    @IBOutlet var contactUsButton: UIButton!
    @IBOutlet var createNewAcBtn: UIButton!
    
    var buttonArray : [UIButton]?
    var buttonTitle = ["Today","7 days","30 days","1 year"]

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        setUIStyle()
        
    }
    
    
    
    func setUIStyle(){
        
        
        //drop labels
        label_1_Drop.attributedText = ThemeEngine.sharedInstance.attributedString(alignment: .center, font: ThemeEngine.sharedInstance.getFontMedium(size: 11), color: UIColor.white, text: "1 year")
        
        label_2_Drop.attributedText = ThemeEngine.sharedInstance.attributedString(alignment: .center, font: ThemeEngine.sharedInstance.getFontMedium(size: 23), color: UIColor.white, text: "104,722 L")
        
        //for time related buttons
        
        buttonArray = [todayButton,weekButton,monthButton,yearButton]
        
        for (index,button) in buttonArray!.enumerated(){
            
            
            
            //normal
            button.setAttributedTitle(ThemeEngine.sharedInstance.attributedString(alignment: .center, font: ThemeEngine.sharedInstance.getFontMedium(size: 12), color: UIColor.primaryBlue, text: buttonTitle[index]), for: .normal)
            button.setBackgroundColor(color: UIColor.white, forState: .normal)
            
            //selected
            button.setAttributedTitle(ThemeEngine.sharedInstance.attributedString(alignment: .center, font: ThemeEngine.sharedInstance.getFontMedium(size: 12), color: UIColor.white, text: buttonTitle[index]), for: .selected)
            button.setBackgroundColor(color: UIColor.primaryBlue, forState: .selected)
            
            //            button.layer.borderWidth = 1
            //            view.layer.borderColor = UIColor.primaryBlue.cgColor
            button.layer.cornerRadius = 12
            button.clipsToBounds = true
            
        }
        buttonArray?.last?.isSelected = true
        
        //tap labels
        
        label_1_Tap.attributedText = ThemeEngine.sharedInstance.attributedString(alignment: .center, font: ThemeEngine.sharedInstance.getFontBold(size: 16), color: UIColor.white, text: "2829")
        
        label_2_Tap.attributedText = ThemeEngine.sharedInstance.attributedString(alignment: .center, font: ThemeEngine.sharedInstance.getFontMedium(size: 12), color: UIColor.white, text: "Line Water")
        
        //glass labels
        
        label_1_Glass.attributedText = ThemeEngine.sharedInstance.attributedString(alignment: .center, font: ThemeEngine.sharedInstance.getFontBold(size: 16), color: UIColor.white, text: "81")
        
        label_2_Glass.attributedText = ThemeEngine.sharedInstance.attributedString(alignment: .center, font: ThemeEngine.sharedInstance.getFontMedium(size: 12), color: UIColor.white, text: "My Water")
        
        
        //bottle labels
        
        label_1_Bottle.attributedText = ThemeEngine.sharedInstance.attributedString(alignment: .center, font: ThemeEngine.sharedInstance.getFontBold(size: 16), color: UIColor.white, text: "73")
        
        label_2_Bottle.attributedText = ThemeEngine.sharedInstance.attributedString(alignment: .center, font: ThemeEngine.sharedInstance.getFontMedium(size: 12), color: UIColor.white, text: "Bottles Saved")
        
        
        
        // createNewAcBtn button
        
        createNewAcBtn.setAttributedTitle(ThemeEngine.sharedInstance.attributedString(alignment: .center, font: ThemeEngine.sharedInstance.getFontMedium(size: 13), color: UIColor.blackTwoTwo, text: "Create an Account"), for: .normal)
        createNewAcBtn.setBackgroundColor(color: UIColor.white, forState: .normal)
        createNewAcBtn.layer.cornerRadius = 4
        createNewAcBtn.clipsToBounds = true
        
        // contactUsButton button
        
        contactUsButton.setAttributedTitle(ThemeEngine.sharedInstance.attributedString(alignment: .right, font: ThemeEngine.sharedInstance.getFontMedium(size: 13), color: UIColor.white, text: "Contact us"), for: .normal)
//        contactUsButton.setBackgroundColor(color: UIColor.white, forState: .normal)
        contactUsButton.leftImage(image: UIImage(named: "ic_contactUs")!, renderMode: .alwaysOriginal)
 
        
        
        
    }
    
    
    @IBAction func timeBtnPressed(_ sender: UIButton) {
        
        buttonArray?.forEach({ (button) in
            button.isSelected = false
        })
        
        for button in buttonArray!{
     
            if button == sender {
                button.isSelected = true
                label_1_Drop.attributedText = ThemeEngine.sharedInstance.attributedString(alignment: .center, font: ThemeEngine.sharedInstance.getFontMedium(size: 11), color: UIColor.white, text: button.currentTitle!)
            }
            
        }
        
        
    }
    

   

}
