//
//  Customer_CreateAccountVC.swift
//  My Water
//
//  Created by Muhammad Mehdi on 07/05/2019.
//  Copyright © 2019 Muhammad Mehdi. All rights reserved.
//

import UIKit
import MaterialComponents

class Customer_CreateAccountVC: BaseViewController {

    
    @IBOutlet var createAccountLabel: UILabel!
    
    
    @IBOutlet var phoneNumberLabel: UILabel!
    @IBOutlet var passwordLabel: UILabel!
    @IBOutlet var fullNameLabel: UILabel!
    @IBOutlet var emailLabel: UILabel!

    
    @IBOutlet var CountryCodeTextField: MDCTextField!
    @IBOutlet var mobileNumberTextField: MDCTextField!
    @IBOutlet var passwordTextField: MDCTextField!
    @IBOutlet var fullNameTextField: MDCTextField!
    @IBOutlet var emailTextField: MDCTextField!

    
     @IBOutlet var createNowBtn: UIButton!
    @IBOutlet var signInBtn: UIButton!

    
    @IBOutlet var alreadyRegisteredLabel: UILabel!
 
    @IBOutlet var phoneNumberErrorLbl: UILabel!
    @IBOutlet var passwordErrorLbl: UILabel!
    @IBOutlet var fullNameErrorLbl: UILabel!
    @IBOutlet var emailErrorLbl: UILabel!

    
    @IBOutlet var textView: UITextView!
    
    //textfield controller
    
    var countryCodeController: MDCTextInputControllerOutlined?
    var mobileNumberController: MDCTextInputControllerOutlined?
    var passwordController: MDCTextInputControllerOutlined?
    var fullNameController: MDCTextInputControllerOutlined?
    var emailController: MDCTextInputControllerOutlined?

    var textFieldArray : [MDCTextField]?
    var textFieldControllerArray : [MDCTextInputControllerOutlined]?
    var textFieldErrorLblArray : [UILabel]?
    var textFieldHeadingLblArray : [UILabel]?
    var placeHolderArray = ["","","00 0000000","",""]
    
    
    var isValidated : Bool = false
    var showPassword = false
    let showPasswordBtn = UIButton(type: .system)

    
    //for action
//    textForTextView.underline(term: "Terms of Use")
//    textForTextView.underline(term: "Privacy Policy")
    let termsOfUse = "Terms of Use"
    let privacyPolicy = "Privacy Policy"

    
    override func viewDidLoad() {
        super.viewDidLoad()

        setUpTextFieldControllers()
        setUIStyle()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    

    func setUpTextFieldControllers(){
        
  
        
        
        countryCodeController = MDCTextInputControllerOutlined(textInput: CountryCodeTextField)
        mobileNumberController = MDCTextInputControllerOutlined(textInput: mobileNumberTextField)
        passwordController = MDCTextInputControllerOutlined(textInput:passwordTextField)
        fullNameController = MDCTextInputControllerOutlined(textInput:fullNameTextField)
        emailController = MDCTextInputControllerOutlined(textInput:emailTextField)

        mobileNumberController?.placeholderText = placeHolderArray.first
        //        mobileNumberController?.isFloatingEnabled = false
        
        
        //        mobileNumberController?.characterCountMax = 10
        //        mobileNumberController?.isFloatingEnabled = false
        
        
      
        textFieldArray = [fullNameTextField,CountryCodeTextField,mobileNumberTextField,emailTextField,passwordTextField]
        textFieldControllerArray = [fullNameController,countryCodeController,mobileNumberController,emailController,passwordController] as! [MDCTextInputControllerOutlined]
        textFieldErrorLblArray = [fullNameErrorLbl,UILabel(), phoneNumberErrorLbl,emailErrorLbl,passwordErrorLbl] as! [UILabel]

        
        textFieldHeadingLblArray = [fullNameLabel,UILabel(), phoneNumberLabel,emailLabel,passwordLabel] as! [UILabel]

        
        for textField in textFieldArray! {
            textField.delegate = self
        }
        
        
        
    }
    
    func setUIStyle(){
        
        //textfield
        
        
        
        for controller in textFieldControllerArray!{
            
            //            controller.underlineViewMode = .never
            //             controller.borderFillColor = UIColor.white
            controller.textInputFont = ThemeEngine.sharedInstance.getFontNormal(size: 14)
            controller.inlinePlaceholderFont = ThemeEngine.sharedInstance.getFontNormal(size: 14)
            
        }
        countryCodeController?.borderFillColor = UIColor.whiteTwo
        
        //add show password button to passwordtextfield
        
        //        button.setImage(UIImage(named: "send.png"), for: .normal)
//        button.setTitle("Show", for: .normal)
        showPasswordBtn.setAttributedTitle(ThemeEngine.sharedInstance.attributedString(alignment: .center, font: ThemeEngine.sharedInstance.getFontNormal(size: 12), color: UIColor.primaryBlue87, text: "Show"), for: .normal)
        showPasswordBtn.setTitleColor(UIColor.primaryBlue, for: .normal)
        //        button.imageEdgeInsets = UIEdgeInsets(top: 0, left: -16, bottom: 0, right: 0)
        showPasswordBtn.frame = CGRect(x: CGFloat(passwordTextField.frame.size.width - 50), y: CGFloat(5), width: CGFloat(50), height: CGFloat(50))
        showPasswordBtn.addTarget(self, action: #selector(self.showHidePassword), for: .touchUpInside)
        passwordTextField.rightView = showPasswordBtn
        passwordTextField.rightViewMode = .always
        passwordTextField.isSecureTextEntry = true
        
        
        //error labels
        
        for labels in textFieldErrorLblArray!{
            labels.attributedText = ThemeEngine.sharedInstance.attributedString(alignment: .left, font: ThemeEngine.sharedInstance.getFontNormal(size: 12), color: UIColor.red, text: "")
            labels.numberOfLines = 2
            labels.lineBreakMode = .byWordWrapping
            labels.adjustsFontSizeToFitWidth = true
            
            labels.isHidden = true
        }
        
        
        
        //sign in labels
        createAccountLabel.attributedText = ThemeEngine.sharedInstance.attributedString(alignment: .center, font: ThemeEngine.sharedInstance.getFontMedium(size: 18), color: UIColor.black, text: "Create an Account")
        
        
        //text field labels
        
     
        fullNameLabel.attributedText = ThemeEngine.sharedInstance.attributedString(alignment: .left, font: ThemeEngine.sharedInstance.getFontNormal(size: 13), color: UIColor.black, text: "Full Name")

        phoneNumberLabel.attributedText = ThemeEngine.sharedInstance.attributedString(alignment: .left, font: ThemeEngine.sharedInstance.getFontNormal(size: 13), color: UIColor.black, text: "Phone Number")

        let emailText = NSMutableAttributedString()
        let email = ThemeEngine.sharedInstance.attributedString(alignment: .left, font: ThemeEngine.sharedInstance.getFontNormal(size: 13), color: UIColor.black, text: "Email")
            
        let optional = ThemeEngine.sharedInstance.attributedString(alignment: .left, font: ThemeEngine.sharedInstance.getFontNormal(size: 13), color: UIColor.brownGreyTwo, text: " (optional)")
        emailText.append(email)
        emailText.append(optional)
        
        
         emailLabel.attributedText = emailText

        passwordLabel.attributedText = ThemeEngine.sharedInstance.attributedString(alignment: .left, font: ThemeEngine.sharedInstance.getFontNormal(size: 13), color: UIColor.black, text: "Password")
        
        
        
        
        createNowBtn.setAttributedTitle(ThemeEngine.sharedInstance.attributedString(alignment: .center, font: ThemeEngine.sharedInstance.getFontMedium(size: 15), color: UIColor.white, text: "Create now"), for: .normal)
        createNowBtn.setBackgroundColor(color: UIColor.primaryBlue, forState: .normal)
        createNowBtn.layer.cornerRadius = 6
        createNowBtn.clipsToBounds = true
        
        
        alreadyRegisteredLabel.attributedText = ThemeEngine.sharedInstance.attributedString(alignment: .center, font: ThemeEngine.sharedInstance.getFontMedium(size: 14), color: UIColor.black, text: "Already registered?")
        
        
        signInBtn.setAttributedTitle(ThemeEngine.sharedInstance.attributedString(alignment: .center, font: ThemeEngine.sharedInstance.getFontMedium(size: 14), color: UIColor.primaryBlue87, text: "Sign in"), for: .normal)
        
        
        
        
        
        //textview
        
        textView.delegate = self
        let textForTextView = NSMutableAttributedString()
        
        
        let temp = ThemeEngine.sharedInstance.attributedString(alignment: .center, font: ThemeEngine.sharedInstance.getFontNormal(size: 12), color: UIColor.blackTwo, text: "By click «Create now», you agree to our Terms of Use and Privacy Policy")
        
        textForTextView.append(temp)
        textForTextView.underline(term: termsOfUse)
        textForTextView.underline(term: privacyPolicy)

//
//        let foundRange = textForTextView.mutableString.range(of: termsOfUse)
//        textForTextView.addAttribute(NSAttributedString.Key.link, value: termsOfUse, range: foundRange)
        
        let foundRange2 = textForTextView.mutableString.range(of: privacyPolicy)
        textForTextView.addAttribute(NSAttributedString.Key.link, value: privacyPolicy, range: foundRange2)
        textView.attributedText = textForTextView

        
    }
    
    @IBAction func showHidePassword(_ sender: Any) {
    
        
        
        
        showPassword.toggle()
        var text = ""
        
        if showPassword{
            passwordTextField.isSecureTextEntry = false
            text = "Hide"
            
        }else{
            passwordTextField.isSecureTextEntry = true

            text = "Show"

            
        }
        showPasswordBtn.setAttributedTitle(ThemeEngine.sharedInstance.attributedString(alignment: .center, font: ThemeEngine.sharedInstance.getFontNormal(size: 12), color: UIColor.primaryBlue87, text: text), for: .normal)

    
    }

}
//MARK:- Textfield delegates

extension Customer_CreateAccountVC: UITextFieldDelegate{
    
    
    
    
    
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        
        
        for (index,controller) in (textFieldControllerArray?.enumerated())!{
            
            if textField == textFieldArray![index] {
                
                controller.placeholderText = ""
                
            }
            
            
            
        }
        
        
        
    }
    
    
    
    func textFieldDidEndEditing(_ textField: UITextField, reason: UITextField.DidEndEditingReason) {
        
        
        
        
        for (index,controller) in (textFieldControllerArray?.enumerated())!{
            
            if textField == textFieldArray![index] {
                
                
                if textField.text!.isEmpty {
                    controller.placeholderText = placeHolderArray[index]
                    
                }else{
                    controller.placeholderText = ""
                }
            }
            
            
            
        }
    }
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        for (index,controller) in (textFieldControllerArray?.enumerated())!{
            if textField == textFieldArray![index] {
                
                UIUtility.sharedInstance.setError(error: false, message: "", textFieldController: controller,textFieldErrorLblArray: textFieldErrorLblArray!,textFieldControllerArray: textFieldControllerArray!)
            }        }
        
        
        return true
    }
    
    
}


//MARK:- textview delegates
extension Customer_CreateAccountVC: UITextViewDelegate{
    
    
    func textView(_ textView: UITextView, shouldInteractWith URL: URL, in characterRange: NSRange, interaction: UITextItemInteraction) -> Bool {
        print("---> function = \(#function) --> Line = \(#line) ")
        
        
        
        return false
        
    }
    
    
  
    
}
