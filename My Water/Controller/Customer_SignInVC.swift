//
//  Customer_SignInVC.swift
//  My Water
//
//  Created by Muhammad Mehdi on 02/05/2019.
//  Copyright © 2019 Muhammad Mehdi. All rights reserved.
//

import UIKit
import MaterialComponents


class Customer_SignInVC: BaseViewController {
    
    
    
    @IBOutlet var signInLabel: UILabel!
    
    
    @IBOutlet var phoneNumberLabel: UILabel!
    @IBOutlet var passwordLabel: UILabel!
    
    
    @IBOutlet var CountryCodeTextField: MDCTextField!
    @IBOutlet var mobileNumberTextField: MDCTextField!
    @IBOutlet var passwordTextField: MDCTextField!
    
    
    @IBOutlet var recoverPasswrdBtn: UIButton!
    @IBOutlet var signInBtn: UIButton!
    
    
    @IBOutlet var newUserLabel: UILabel!
    @IBOutlet var createAccountBtn: UIButton!
    
    @IBOutlet var phoneNumberErrorLbl: UILabel!
    @IBOutlet var passwordErrorLbl: UILabel!
    
    
    
    
    //textfield controller
    
    var countryCodeController: MDCTextInputControllerOutlined?
    var mobileNumberController: MDCTextInputControllerOutlined?
    var passwordController: MDCTextInputControllerOutlined?
    
    var textFieldArray : [MDCTextField]?
    var textFieldControllerArray : [MDCTextInputControllerOutlined]?
    var textFieldErrorLblArray : [UILabel]?
    var placeHolderArray = ["1","00 0000000","2"]
    var isValidated : Bool = false
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setUpTextFieldControllers()
        setUIStyle()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    
    
    func setUpTextFieldControllers(){
        
        
        
        countryCodeController = MDCTextInputControllerOutlined(textInput: CountryCodeTextField)
        mobileNumberController = MDCTextInputControllerOutlined(textInput: mobileNumberTextField)
        passwordController = MDCTextInputControllerOutlined(textInput:passwordTextField)
        
        mobileNumberController?.placeholderText = placeHolderArray[1]
        //        mobileNumberController?.isFloatingEnabled = false
        
        
        //        mobileNumberController?.characterCountMax = 10
        //        mobileNumberController?.isFloatingEnabled = false
        
        
        
        
        CountryCodeTextField.delegate = self
        mobileNumberTextField.delegate = self
        passwordTextField.delegate = self
        
        textFieldArray = [CountryCodeTextField,mobileNumberTextField,passwordTextField]
        textFieldControllerArray = [countryCodeController,mobileNumberController,passwordController] as! [MDCTextInputControllerOutlined]
        textFieldErrorLblArray = [UILabel(), phoneNumberErrorLbl,passwordErrorLbl] as! [UILabel]
        
        
        
        
        
        
    }
    
    func setUIStyle(){
        
        //textfield
        
        
        
        for controller in textFieldControllerArray!{
            
            //            controller.underlineViewMode = .never
            //             controller.borderFillColor = UIColor.white
            controller.textInputFont = ThemeEngine.sharedInstance.getFontNormal(size: 14)
            controller.inlinePlaceholderFont = ThemeEngine.sharedInstance.getFontNormal(size: 14)
            
        }
        countryCodeController?.borderFillColor = UIColor.whiteTwo
        
        //error labels
        
        for labels in textFieldErrorLblArray!{
            labels.attributedText = ThemeEngine.sharedInstance.attributedString(alignment: .left, font: ThemeEngine.sharedInstance.getFontNormal(size: 12), color: UIColor.red, text: "")
            labels.numberOfLines = 2
            labels.lineBreakMode = .byWordWrapping
            labels.adjustsFontSizeToFitWidth = true
            
            labels.isHidden = true
        }
        
        
        
        //sign in labels
        signInLabel.attributedText = ThemeEngine.sharedInstance.attributedString(alignment: .center, font: ThemeEngine.sharedInstance.getFontMedium(size: 18), color: UIColor.black, text: "Sign In")
        
        phoneNumberLabel.attributedText = ThemeEngine.sharedInstance.attributedString(alignment: .left, font: ThemeEngine.sharedInstance.getFontNormal(size: 13), color: UIColor.black, text: "Phone Number")
        
        passwordLabel.attributedText = ThemeEngine.sharedInstance.attributedString(alignment: .left, font: ThemeEngine.sharedInstance.getFontNormal(size: 13), color: UIColor.black, text: "Password")
        
        
        recoverPasswrdBtn.setAttributedTitle(ThemeEngine.sharedInstance.attributedString(alignment: .center, font: ThemeEngine.sharedInstance.getFontMedium(size: 12), color: UIColor.brownGrey, text: "Recover password"), for: .normal)
        
        signInBtn.setAttributedTitle(ThemeEngine.sharedInstance.attributedString(alignment: .center, font: ThemeEngine.sharedInstance.getFontMedium(size: 15), color: UIColor.white, text: "Sign in"), for: .normal)
        signInBtn.setBackgroundColor(color: UIColor.primaryBlue, forState: .normal)
        signInBtn.layer.cornerRadius = 6
        signInBtn.clipsToBounds = true
        
        
        newUserLabel.attributedText = ThemeEngine.sharedInstance.attributedString(alignment: .center, font: ThemeEngine.sharedInstance.getFontMedium(size: 14), color: UIColor.black, text: "New user?")
        
        
        createAccountBtn.setAttributedTitle(ThemeEngine.sharedInstance.attributedString(alignment: .center, font: ThemeEngine.sharedInstance.getFontMedium(size: 14), color: UIColor.primaryBlue87, text: "Create an account"), for: .normal)
        
    }
    
    //MARK:- Button Presses
    
    
    
    @IBAction func buttonPressed(_ sender: UIButton) {
        
        switch sender {
        case recoverPasswrdBtn:
            //            mobileNumberController?.setErrorText("Oops, we don't have an account with this phone. Would you like to register an account?", errorAccessibilityValue: nil)
            UIUtility.sharedInstance.setError(error: true, message: "Oops, we don't have an account with this phone. Would you like to register an account?", textFieldController: mobileNumberController!, textFieldErrorLblArray: textFieldErrorLblArray!, textFieldControllerArray: textFieldControllerArray!)
            
            
        case signInBtn:
            //            passwordController?.setErrorText("Oops, we don't have an account with this phone. Would you like to register an account?", errorAccessibilityValue: nil)
//            UIUtility.sharedInstance.setError(error: true, message: "Oops, we don't have an account with this phone. Would you like to register an account?", textFieldController: passwordController!,textFieldErrorLblArray: textFieldErrorLblArray!, textFieldControllerArray: textFieldControllerArray!)
            
//            self.present(UIUtility.sharedInstance.instantiateVC(storyboard: "Customer_PasswordRecovery", identifier: "Customer_PasswordRecoveryVC"), animated: true, completion: nil)
            
            self.navigationController?.pushViewController(UIUtility.sharedInstance.instantiateVC(storyboard: "Customer_PasswordRecovery", identifier: "Customer_PasswordRecoveryVC"), animated: true)
            
        case createAccountBtn:
            self.navigationController?.pushViewController(UIUtility.sharedInstance.instantiateVC(storyboard: "Customer_CreateAccount", identifier: "Customer_CreateAccountVC"), animated: true)

        default:
            print()
        }
        
        
        
    }
    
    
    
    
}


//MARK:- Textfield delegates

extension Customer_SignInVC: UITextFieldDelegate{
    
    
    
    
 
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        
        
        for (index,controller) in (textFieldControllerArray?.enumerated())!{
            
            if textField == textFieldArray![index] {
                
                controller.placeholderText = ""
                
            }
            
            
            
        }
        
        
        
    }
    
    
    
    func textFieldDidEndEditing(_ textField: UITextField, reason: UITextField.DidEndEditingReason) {
        
        
        
        
        for (index,controller) in (textFieldControllerArray?.enumerated())!{
            
            if textField == textFieldArray![index] {
                
                
                if textField.text!.isEmpty {
                    controller.placeholderText = placeHolderArray[index]
                    
                }else{
                    controller.placeholderText = ""
                }
            }
            
            
            
        }
    }
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        for (index,controller) in (textFieldControllerArray?.enumerated())!{
            if textField == textFieldArray![index] {
                
                UIUtility.sharedInstance.setError(error: false, message: "", textFieldController: controller,textFieldErrorLblArray: textFieldErrorLblArray!,textFieldControllerArray: textFieldControllerArray!)
            }        }
        
        
        return true
    }
    
    
}
