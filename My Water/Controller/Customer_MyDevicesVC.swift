//
//  Customer_MyDevicesVC.swift
//  My Water
//
//  Created by Muhammad Mehdi on 09/05/2019.
//  Copyright © 2019 Muhammad Mehdi. All rights reserved.
//

import UIKit
import MaterialComponents


class Customer_MyDevicesVC: BaseViewController {
    
    
    @IBOutlet var noDevicesStackView: UIStackView!
    @IBOutlet var firstLabel: UILabel!
    @IBOutlet var contactSalesButton: UIButton!
    
    
    
    @IBOutlet var tableView: UITableView!
    @IBOutlet var addDeviceButton: MDCFloatingButton!
    
    var sideMenuOptions_label = ["Scan for quality","My devices","Reports","Pay my bill"]
    

    override func viewDidLoad() {
        super.viewDidLoad()

        
        super.addSideMenu()
        super.showLeftLabel(show: true, heading: "My Devices")
        super.showLeftMenu(show: true)
        super.setNavBarButtons(selfObj: self)

        
        

        setUIStyle()

        noDevicesStackView.isHidden = true
        tableView.delegate = self
        tableView.dataSource = self



    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationItem.setHidesBackButton(true, animated:true);
    }
    

    func setUIStyle(){
        
    //add button
        // Note: you'll need to provide your own image - the following is just an example.
        
        let plusImage = UIImage(named: "ic_addDevice")!.withRenderingMode(.alwaysOriginal)
        addDeviceButton.setImage(plusImage, for: .normal)
        
        
        
    }

    
 
}
//MARK:- tableview delegates
extension Customer_MyDevicesVC: UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return sideMenuOptions_label.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "Customer_MyDevicesTVCell", for: indexPath) as? Customer_MyDevicesTVCell {
            
          cell.mapDataOnCell(cell: cell, indexPath: indexPath, array: sideMenuOptions_label)
            
            cell.selectionStyle = .none
            return cell
        }
        
        return Customer_MyDevicesTVCell()
    }
    
 
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
}
