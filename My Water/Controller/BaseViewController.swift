//
//  BaseViewController.swift
//  My Water
//
//  Created by Muhammad Mehdi on 02/05/2019.
//  Copyright © 2019 Muhammad Mehdi. All rights reserved.
//

import UIKit
import SnapKit
import SideMenu


var sideMenuPanGesture  = UIPanGestureRecognizer()
var sideMenuedgeGesture = [UIScreenEdgePanGestureRecognizer]()


class BaseViewController: UIViewController {
    
    
    
    let leftLabel = UILabel()
    let leftMenuButton = UIButton()
    @objc dynamic var SELF:AnyObject! //self object of the class from where we are calling it
    var className:String = ""
    
    
    
    var notificationImage = #imageLiteral(resourceName: "ic_notificationAlert")
    
    var isNotificationActive = true


    override func viewDidLoad() {
        super.viewDidLoad()

        setNavBarStyle()
        keyboardDismissGesture()
//         print("visibleViewController \(self.navigationController?.visibleViewController)")
        
        
        
     
        
       
     }
    
    
    func setNavBarButtons(selfObj:AnyObject){
        SELF = selfObj
        className = String(describing: SELF.classForCoder!)
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
//        print("classname \(className)")
        
//        add right notification icon
        if className == "Customer_MyDevicesVC"
        {
            let notificationButton = UIButton()
            
            notificationButton.addTarget(self, action: #selector(didTapRightButton), for: .touchUpInside)
            
        
            if isNotificationActive{
                notificationImage = #imageLiteral(resourceName: "ic_notificationAlert")
            }else{
                notificationImage = #imageLiteral(resourceName: "ic_notificationAlertInactive")
            }
            
            notificationButton.setImage(notificationImage, for: .normal)
            notificationButton.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
            notificationButton.widthAnchor.constraint(equalToConstant: 30).isActive = true
            notificationButton.heightAnchor.constraint(equalToConstant: 30).isActive = true
            
//            SELF.navigationItem.setRightBarButton(UIBarButtonItem(customView: notificationButton), animated: true)
            self.navigationItem.setRightBarButton(UIBarButtonItem(customView: notificationButton), animated: true)
            
            
        }
        
        
        
        if className == "Customer_PayMyBillVC"
        {
            let rightButton = UIButton()
            
            rightButton.addTarget(self, action: #selector(didTapRightButton), for: .touchUpInside)
            
            
           
            rightButton.setImage(#imageLiteral(resourceName: "ic_calendar"), for: .normal)
            rightButton.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
            rightButton.widthAnchor.constraint(equalToConstant: 30).isActive = true
            rightButton.heightAnchor.constraint(equalToConstant: 30).isActive = true
         
            self.navigationItem.setRightBarButton(UIBarButtonItem(customView: rightButton), animated: true)
            
            
        }
     
        
        
    }
    
    
    @objc func didTapRightButton(sender: AnyObject) {
        if className == "Customer_MyDevicesVC"
        {
            if isNotificationActive {
                self.navigationController?.pushViewController(UIUtility.sharedInstance.instantiateVC(storyboard: "Customer_Notifications", identifier: "Customer_NotificationsVC"), animated: true)
            }
            
            
        }
        
        
        
        if className == "Customer_PayMyBillVC"
        {
            print("calendar tapped")
            
            
        }
    }
    
 
    func showLeftMenu(show: Bool){
        
        leftMenuButton.isHidden = !show
        
    }
    
    
    
    
    func showLeftLabel(show: Bool, heading: String){
        
        //left menu button
        
        leftMenuButton.isHidden = true
        leftMenuButton.setImage(#imageLiteral(resourceName: "ic_sideMenu"), for: .normal)
//        leftMenuButton.tintColor = UIColor.black
        //        menuButton.imageView?.contentMode = .scaleAspectFit
        leftMenuButton.frame = CGRect(x: 0, y: 0, width: 45, height: 40)
        leftMenuButton.addTarget(self, action: #selector(didTapLeftMenuButton), for: .touchUpInside)
        
        
        //        menuButton.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 0, -20);
        //        logOutButton.contentEdgeInsets = UIEdgeInsetsMake(0, 0, -8, -20);
        
        leftMenuButton.widthAnchor.constraint(equalToConstant: 45).isActive = true
        leftMenuButton.heightAnchor.constraint(equalToConstant: 40).isActive = true
        
        //        let spacer = UIBarButtonItem(barButtonSystemItem: .fixedSpace, target: nil, action: nil)
        //        spacer.width = -20
        
 
        
  
        
        
//        let leftLabel = UILabel(frame: CGRect(x: 50, y: 0, width: self.view.frame.width / 3 , height: 50))
        
        
        //add left label
        leftLabel.frame = CGRect(x: 50, y: 0, width: self.view.frame.width / 3 , height: 50)
        
             leftLabel.attributedText = ThemeEngine.sharedInstance.attributedString(alignment: .center, font: ThemeEngine.sharedInstance.getFontMedium(size: 18), color: UIColor.black, text: heading)
        
        
        
        
        self.navigationItem.setLeftBarButtonItems([ UIBarButtonItem(customView: leftMenuButton),  UIBarButtonItem(customView: leftLabel)], animated: true)

        leftLabel.isHidden = !show

    }
    
    
    @objc func didTapLeftMenuButton(sender: AnyObject) {
        
        if SideMenuManager.default.menuLeftNavigationController != nil {
            present(SideMenuManager.default.menuLeftNavigationController!, animated: true, completion: nil)
            
        }
    }
    

    
    

    func setNavBarStyle(){
//        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.shadowColor = UIColor(red: 0/255, green: 0/255, blue: 0/255, alpha: 0.1)
        self.navigationController?.navigationBar.shadowOpacity = 1

        self.navigationController?.navigationBar.shadowOffset = CGSize(width: 0, height: 6)
        self.navigationController?.navigationBar.shadowRadius = 10 / 2
        
        
        //back image
        
        let yourBackImage = UIImage(named: "backArrow")
        self.navigationController?.navigationBar.backIndicatorImage = yourBackImage
        self.navigationController?.navigationBar.backIndicatorTransitionMaskImage = yourBackImage
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: UIBarButtonItem.Style.plain, target: nil, action: nil)
        
        
        self.navigationController?.navigationBar.tintColor = #colorLiteral(red: 0.7490196078, green: 0.7490196078, blue: 0.7490196078, alpha: 1)
        
        
        //left button with back bar
        self.navigationItem.leftItemsSupplementBackButton = true

 
    }
    
    
    func keyboardDismissGesture(){
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: "dismissKeyboard")
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    
    
    
    //MARK:- sidemenu
    func addSideMenu(){
        
        
        let storyboard = UIStoryboard(name: "Customer_SideMenu", bundle: Bundle.main)
        let menuRightNavigationController = storyboard.instantiateViewController(withIdentifier: "SideMenuNavController") as! UISideMenuNavigationController
        
        
        SideMenuManager.default.menuLeftNavigationController = menuRightNavigationController
        sideMenuPanGesture =  SideMenuManager.default.menuAddPanGestureToPresent(toView: self.navigationController!.navigationBar)
        sideMenuedgeGesture = SideMenuManager.default.menuAddScreenEdgePanGesturesToPresent(toView: self.navigationController!.view)
        SideMenuManager.default.menuPresentMode = .menuSlideIn
        
        SideMenuManager.default.menuFadeStatusBar = false
        SideMenuManager.default.menuAnimationFadeStrength = 0.5
        
        SideMenuManager.default.menuAllowPushOfSameClassTwice = false
    }
    
    
    
    
    func disableSideMenu() {
        print("---> Line = \(#line) in function = \(#function)")
        
        //        SideMenuManager.default.menuWidth = 0.0
        //        SideMenuManager.defaultManager.menuWidth = 0.0
        
        sideMenuPanGesture.isEnabled = false
        sideMenuedgeGesture.forEach{$0.isEnabled = false}
    }
    
    // enable side menu
    func enableSideMenu() {
        //        SideMenuManager.default.menuWidth = 250.0
        sideMenuPanGesture.isEnabled = true
        sideMenuedgeGesture.forEach{$0.isEnabled = true}
    }

}
