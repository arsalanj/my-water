//
//  Customer_SideMenuVC.swift
//  My Water
//
//  Created by Muhammad Mehdi on 09/05/2019.
//  Copyright © 2019 Muhammad Mehdi. All rights reserved.
//

import UIKit
import MaterialComponents

class Customer_SideMenuVC: UIViewController {
    
    
    @IBOutlet var nameLabel: UILabel!
    @IBOutlet var tableView: UITableView!
    
    @IBOutlet var payMyBillButton: UIButton!
    @IBOutlet var signOutButton: UIButton!
    
    
    var sideMenuOptions_label = ["Scan for quality","My devices","Reports","Pay my bill","Profile","Reset password","Contact us","Referral Program"]
    let sideMenuOptions_images = ["ic_ScanForQuality","ic_MyDevices","ic_Reports","ic_PayMyBill","ic_Profile","ic_ResetPassword","ic_ContactUs-1","ic_ReferralProgram"]
    

    override func viewDidLoad() {
        super.viewDidLoad()

        
        self.tableView.delegate = self
        self.tableView.dataSource = self
        setUIStyle()

        self.tableView.reloadData()
//        tableView.estimatedRowHeight = 80.0
//        tableView.rowHeight = UITableView.automaticDimension
        
    }
    
 
    
    
    func setUIStyle(){

        //name label
        nameLabel.attributedText = ThemeEngine.sharedInstance.attributedString(alignment: .left, font: ThemeEngine.sharedInstance.getFontMedium(size: 13), color: UIColor.white, text: "Hello, User")
        nameLabel.adjustsFontSizeToFitWidth = true
        
        
        //pay my bill button
        payMyBillButton.isHidden = true
        payMyBillButton.layer.borderWidth = 1
        payMyBillButton.layer.borderColor = UIColor.red.cgColor
        payMyBillButton.layer.cornerRadius = 4
        
    payMyBillButton.setAttributedTitle(ThemeEngine.sharedInstance.attributedString(alignment: .right, font: ThemeEngine.sharedInstance.getFontMedium(size: 13), color: UIColor.red, text: "Pay My Bill"), for: .normal)
        //        contactUsButton.setBackgroundColor(color: UIColor.white, forState: .normal)
        payMyBillButton.leftImage(image: UIImage(named: "ic_error")!, renderMode: .alwaysOriginal)
        
        
        signOutButton.setAttributedTitle(ThemeEngine.sharedInstance.attributedString(alignment: .right, font: ThemeEngine.sharedInstance.getFontMedium(size: 13), color: UIColor.blackTwoTwo, text: "Sign Out"), for: .normal)

        
        
    }

}
//MARK:- tableview delegates
extension Customer_SideMenuVC: UITableViewDelegate, UITableViewDataSource{
    
    override func viewDidLayoutSubviews() {
        tableView.frame.size = tableView.contentSize
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return sideMenuOptions_label.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "Customer_SideMenuTVCell", for: indexPath) as? Customer_SideMenuTVCell {
            
            cell.label.attributedText = ThemeEngine.sharedInstance.attributedString(alignment: .left, font: ThemeEngine.sharedInstance.getFontMedium(size: 13), color: UIColor.blackTwoTwo, text: sideMenuOptions_label[indexPath.row])
           cell.label.adjustsFontSizeToFitWidth = true

            cell.logo.image = UIImage(named: sideMenuOptions_images[indexPath.row])
            
            cell.selectionStyle = .none
            return cell
        }
        
        return Customer_SideMenuTVCell()
    }
    
    
 
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        print("indexPath.row \(indexPath.row)")
        
        switch indexPath.row {
        case 3:
            print()
            self.navigationController?.pushViewController(UIUtility.sharedInstance.instantiateVC(storyboard: "Customer_PayMyBill", identifier: "Customer_PayMyBillVC"), animated: true)
            
            
        default:
            print()
            
            
        }
    }
}
