//
//  Customer_NotificationsVC.swift
//  My Water
//
//  Created by Muhammad Mehdi on 10/05/2019.
//  Copyright © 2019 Muhammad Mehdi. All rights reserved.
//

import UIKit

class Customer_NotificationsVC: BaseViewController {
    
    
    @IBOutlet var tableView: UITableView!
    @IBOutlet var noMoreLabel: UILabel!
    
    var sideMenuOptions_label = ["Scan for quality","My devices","Reports","Pay my bill"]


    override func viewDidLoad() {
        super.viewDidLoad()

        super.showLeftLabel(show: true, heading: "Notifications")
        super.showLeftMenu(show: false)
        
        
        setUIStyle()

        tableView.delegate = self
        tableView.dataSource = self
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationItem.setHidesBackButton(false, animated:true);
    }
    
    
    
    
    func setUIStyle(){
        
        noMoreLabel.attributedText = ThemeEngine.sharedInstance.attributedString(alignment: .center, font: ThemeEngine.sharedInstance.getFontNormal(size: 12), color: UIColor.brownGreyTwo, text: "No more")
        noMoreLabel.adjustsFontSizeToFitWidth = true
        
        
    }

 

}


//MARK:- tableview delegates
extension Customer_NotificationsVC: UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return sideMenuOptions_label.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "Customer_NotificationsTVCell", for: indexPath) as? Customer_NotificationsTVCell {
            
            cell.mapDataOnCell(cell: cell, indexPath: indexPath, array: sideMenuOptions_label)
            
            cell.selectionStyle = .none
            return cell
        }
        
        return Customer_NotificationsTVCell()
    }
    
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
}
