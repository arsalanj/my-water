//
//  CustomerGetStartedVC.swift
//  My Water
//
//  Created by Muhammad Mehdi on 30/04/2019.
//  Copyright © 2019 Muhammad Mehdi. All rights reserved.
//

import UIKit
import MaterialComponents
import SnapKit

class CustomerGetStartedVC: UIViewController {
    
    @IBOutlet var buttonStackView: UIStackView!
    
    @IBOutlet var createAccountBtn: UIButton!
    @IBOutlet var alreadyHaveAccntBtn: UIButton!
    @IBOutlet var continueAsGuestBtn: UIButton!
    
    
    @IBOutlet var scrollView: UIScrollView!
    @IBOutlet var pageControl: MDCPageControl!
    var slides:[OnboardingView] = [];

    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        setUpUIStyle()
        
        
        slides = createSlides()
        setupSlideScrollView(slides: slides)
        
        pageControl.numberOfPages = slides.count
        pageControl.currentPage = 0
        view.bringSubviewToFront(pageControl)
        
        
        
        
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: animated)

        
        
    }
    
    func setUpUIStyle(){
        
        scrollView.delegate = self
        
        pageControl.currentPageIndicatorTintColor = UIColor.primaryBlue
//        pageControl.pageIndicatorTintColor = UIColor.primaryBlue

        createAccountBtn.layer.cornerRadius = 6
        createAccountBtn.backgroundColor = UIColor.primaryBlue
        createAccountBtn.setAttributedTitle(ThemeEngine.sharedInstance.attributedString(alignment: .center, font: ThemeEngine.sharedInstance.getFontMedium(size: 15), color: UIColor.white, text: "Create an account"), for: .normal)
        
        
        alreadyHaveAccntBtn.layer.cornerRadius = 6
        alreadyHaveAccntBtn.backgroundColor = UIColor.veryLightPink
        alreadyHaveAccntBtn.setAttributedTitle(ThemeEngine.sharedInstance.attributedString(alignment: .center, font: ThemeEngine.sharedInstance.getFontMedium(size: 15), color: UIColor.blackTwo, text: "I already have an account"), for: .normal)
        
        continueAsGuestBtn.layer.cornerRadius = 6
        continueAsGuestBtn.backgroundColor = UIColor.clear
        continueAsGuestBtn.setAttributedTitle(ThemeEngine.sharedInstance.attributedString(alignment: .center, font: ThemeEngine.sharedInstance.getFontMedium(size: 15), color: UIColor.primaryBlue87, text: "Continue as guest"), for: .normal)
        
        
 
        
        
    }
    
    
    func createSlides() -> [OnboardingView] {

   

        let slide1:OnboardingView = Bundle.main.loadNibNamed("OnboardingView", owner: self, options: nil)?.first as! OnboardingView
        slide1.topImageView.image = UIImage(named: "group4")
        slide1.mainImageView.image = UIImage(named: "group_1")
//        slide1.label.text = "Purify water when you need it"
        slide1.label.attributedText = ThemeEngine.sharedInstance.attributedString(alignment: .center, font: ThemeEngine.sharedInstance.getFontMedium(size: 16), color: UIColor.black87, text: "Purify water when you need it")


        let slide2:OnboardingView = Bundle.main.loadNibNamed("OnboardingView", owner: self, options: nil)?.first as! OnboardingView
        slide2.topImageView.image = UIImage(named: "group4")
        slide2.mainImageView.image = UIImage(named: "group_2")
//        slide2.label.text = "Purify water when you need it"
        slide2.label.attributedText = ThemeEngine.sharedInstance.attributedString(alignment: .center, font: ThemeEngine.sharedInstance.getFontMedium(size: 16), color: UIColor.black87, text: "No maintenance hassle")

        
        let slide3:OnboardingView = Bundle.main.loadNibNamed("OnboardingView", owner: self, options: nil)?.first as! OnboardingView
        slide3.topImageView.image = UIImage(named: "group4")
        slide3.mainImageView.image = UIImage(named: "group_3")
//        slide3.label.text = "Purify water when you need it"
        slide3.label.attributedText = ThemeEngine.sharedInstance.attributedString(alignment: .center, font: ThemeEngine.sharedInstance.getFontMedium(size: 16), color: UIColor.black87, text: "Monitor quality of every drop")

        
        let slide4:OnboardingView = Bundle.main.loadNibNamed("OnboardingView", owner: self, options: nil)?.first as! OnboardingView
        slide4.topImageView.image = UIImage(named: "group4")
        slide4.mainImageView.image = UIImage(named: "group_4")
//        slide4.label.text = "Purify water when you need it"
        slide4.label.attributedText = ThemeEngine.sharedInstance.attributedString(alignment: .center, font: ThemeEngine.sharedInstance.getFontMedium(size: 16), color: UIColor.black87, text: "Reduce plastic waste")



        return [slide1,slide2,slide3,slide4]
    }
    
    
    func setupSlideScrollView(slides : [OnboardingView]) {
        scrollView.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: scrollView.frame.height)
 
        scrollView.contentSize = CGSize(width: view.frame.width * CGFloat(slides.count), height: scrollView.frame.height)
        scrollView.isPagingEnabled = true
        self.scrollView.contentSize.height = 1.0 // disable vertical scroll

        
        for i in 0 ..< slides.count {
            slides[i].frame = CGRect(x: view.frame.width * CGFloat(i), y: 0, width: view.frame.width, height: scrollView.frame.height)
            scrollView.addSubview(slides[i])
            
        }
    }
    
    
    //MARK:- button press actions
    
    @IBAction func buttonPressed(_ sender: UIButton) {
        
        
        switch sender {
        case createAccountBtn:
            self.navigationController?.pushViewController(UIUtility.sharedInstance.instantiateVC(storyboard: "Customer_CreateAccount", identifier: "Customer_CreateAccountVC"), animated: true)

        case alreadyHaveAccntBtn:
            print()
//            self.present(UIUtility.sharedInstance.instantiateVC(storyboard: "Customer_SignIn", identifier: "Customer_SignInVC"), animated: true, completion: nil)
            
            self.navigationController?.pushViewController(UIUtility.sharedInstance.instantiateVC(storyboard: "Customer_SignIn", identifier: "Customer_SignInVC"), animated: true)
            
        case continueAsGuestBtn:
            print()

        default:
            print()
        }
        
        
    }
    
    

}


extension CustomerGetStartedVC: UIScrollViewDelegate{
    
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let pageIndex = round(scrollView.contentOffset.x/view.frame.width)
        pageControl.currentPage = Int(pageIndex)
        
//        let maximumHorizontalOffset: CGFloat = scrollView.contentSize.width - scrollView.frame.width
//        let currentHorizontalOffset: CGFloat = scrollView.contentOffset.x
        
        /*
        // vertical
        let maximumVerticalOffset: CGFloat = scrollView.contentSize.height - scrollView.frame.height
        let currentVerticalOffset: CGFloat = scrollView.contentOffset.y
        
        let percentageHorizontalOffset: CGFloat = currentHorizontalOffset / maximumHorizontalOffset
        let percentageVerticalOffset: CGFloat = currentVerticalOffset / maximumVerticalOffset
        
        
        /*
         * below code changes the background color of view on paging the scrollview
         */
        //        self.scrollView(scrollView, didScrollToPercentageOffset: percentageHorizontalOffset)
        
        
        /*
         * below code scales the imageview on paging the scrollview
         */
        let percentOffset: CGPoint = CGPoint(x: percentageHorizontalOffset, y: percentageVerticalOffset)
        
        if(percentOffset.x > 0 && percentOffset.x <= 0.25) {
            
            slides[0].mainImageView.transform = CGAffineTransform(scaleX: (0.25-percentOffset.x)/0.25, y: (0.25-percentOffset.x)/0.25)
            slides[1].mainImageView.transform = CGAffineTransform(scaleX: percentOffset.x/0.25, y: percentOffset.x/0.25)
            
        } else if(percentOffset.x > 0.25 && percentOffset.x <= 0.50) {
            slides[1].mainImageView.transform = CGAffineTransform(scaleX: (0.50-percentOffset.x)/0.25, y: (0.50-percentOffset.x)/0.25)
            slides[2].mainImageView.transform = CGAffineTransform(scaleX: percentOffset.x/0.50, y: percentOffset.x/0.50)
            
        } else if(percentOffset.x > 0.50 && percentOffset.x <= 0.75) {
            slides[2].mainImageView.transform = CGAffineTransform(scaleX: (0.75-percentOffset.x)/0.25, y: (0.75-percentOffset.x)/0.25)
            slides[3].mainImageView.transform = CGAffineTransform(scaleX: percentOffset.x/0.75, y: percentOffset.x/0.75)
            
        } else if(percentOffset.x > 0.75 && percentOffset.x <= 1) {
            slides[3].mainImageView.transform = CGAffineTransform(scaleX: (1-percentOffset.x)/0.25, y: (1-percentOffset.x)/0.25)
//            slides[4].mainImageView.transform = CGAffineTransform(scaleX: percentOffset.x, y: percentOffset.x)
        }
    
    */
    
    
    }
}


