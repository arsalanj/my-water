//
//  Customer_BillPayCVCell.swift
//  My Water
//
//  Created by Muhammad Mehdi on 13/05/2019.
//  Copyright © 2019 Muhammad Mehdi. All rights reserved.
//

import UIKit

class Customer_BillPayCVCell: UICollectionViewCell {
    
    @IBOutlet var imageView: UIImageView!
    @IBOutlet var mainView: UIView!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
//        mainView.layer.borderWidth = 1
        mainView.layer.borderColor = UIColor.clear.cgColor
//        mainView.layer.cornerRadius = 6
        mainView.layer.borderWidth = 1
        mainView.layer.borderColor = UIColor.veryLightPinkFive.cgColor
        mainView.layer.cornerRadius = 6
        mainView.layer.shadowColor = UIColor.black2.cgColor
        mainView.layer.shadowOpacity = 1
        mainView.layer.shadowOffset = CGSize(width: 0, height: 6)
        mainView.layer.shadowRadius = 10 / 2
        mainView.layer.shadowPath = nil
        
    }
    
    
    
}
