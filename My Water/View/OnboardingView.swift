//
//  OnboardingView.swift
//  My Water
//
//  Created by Muhammad Mehdi on 30/04/2019.
//  Copyright © 2019 Muhammad Mehdi. All rights reserved.
//

import Foundation
import UIKit



class OnboardingView : UIView {
    
    @IBOutlet var label: UILabel!
    
    @IBOutlet var topImageView: UIImageView!
    @IBOutlet var mainImageView: UIImageView!
    
    
}
