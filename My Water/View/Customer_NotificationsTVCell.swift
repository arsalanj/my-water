//
//  Customer_NotificationsTVCell.swift
//  My Water
//
//  Created by Muhammad Mehdi on 10/05/2019.
//  Copyright © 2019 Muhammad Mehdi. All rights reserved.
//

import UIKit

class Customer_NotificationsTVCell: UITableViewCell {

    
    @IBOutlet var logoImage: UIImageView!
    @IBOutlet var firstLabel: UILabel!
    @IBOutlet var dateLabel: UILabel!
    
    @IBOutlet var arrowButton: UIButton!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    
    func mapDataOnCell(cell: Customer_NotificationsTVCell?, indexPath: IndexPath,array : [String]) {

        
        switch indexPath.row {
        case 0 :
            print()
            logoImage.image = #imageLiteral(resourceName: "iconNotificationStoppedActivity")
            firstLabel.attributedText = ThemeEngine.sharedInstance.attributedString(alignment: .center, font: ThemeEngine.sharedInstance.getFontNormal(size: 12), color: UIColor.black, text: "Your device [ID# or name] stopped activity.")
            dateLabel.attributedText = ThemeEngine.sharedInstance.attributedString(alignment: .left, font: ThemeEngine.sharedInstance.getFontNormal(size: 12), color: UIColor.brownGreyTwo, text: "06/8/2019")
            
        case 1:
            print()
            logoImage.image = #imageLiteral(resourceName: "iconNotificationCompleted")
            firstLabel.attributedText = ThemeEngine.sharedInstance.attributedString(alignment: .center, font: ThemeEngine.sharedInstance.getFontNormal(size: 12), color: UIColor.black, text: "Your device [ID# or name] stopped activity.")
            dateLabel.attributedText = ThemeEngine.sharedInstance.attributedString(alignment: .left, font: ThemeEngine.sharedInstance.getFontNormal(size: 12), color: UIColor.brownGreyTwo, text: "06/8/2019")
            
            
        case 2:
            print()
            logoImage.image = #imageLiteral(resourceName: "iconNotificationNeedReplace")
            firstLabel.attributedText = ThemeEngine.sharedInstance.attributedString(alignment: .center, font: ThemeEngine.sharedInstance.getFontNormal(size: 12), color: UIColor.black, text: "Filtering in device [ID# or name] is not effective, needs replacement.")
            dateLabel.attributedText = ThemeEngine.sharedInstance.attributedString(alignment: .left, font: ThemeEngine.sharedInstance.getFontNormal(size: 12), color: UIColor.brownGreyTwo, text: "06/8/2019")
            
        default:
            print()
            
            
            
            
        }
        
        
        
        
        
    }
    
    //MARK:- button action
    @IBAction func arrowBtnPressed(_ sender: UIButton) {
        
        
    }
    
    
    

}
