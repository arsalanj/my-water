//
//  Customer_SideMenuTVCell.swift
//  My Water
//
//  Created by Muhammad Mehdi on 09/05/2019.
//  Copyright © 2019 Muhammad Mehdi. All rights reserved.
//

import UIKit




class Customer_SideMenuTVCell: UITableViewCell {
 
    @IBOutlet var logo: UIImageView!
    @IBOutlet var label: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
