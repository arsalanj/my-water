//
//  Customer_PayMyBillTVCell.swift
//  My Water
//
//  Created by Muhammad Mehdi on 13/05/2019.
//  Copyright © 2019 Muhammad Mehdi. All rights reserved.
//

import UIKit

class Customer_PayMyBillTVCell: UITableViewCell {
    
    
    @IBOutlet var dateLabel: UILabel!
    @IBOutlet var bundleLabel: UILabel!
    @IBOutlet var amountLabel: UILabel!
    @IBOutlet var payNow: UILabel!
    
    @IBOutlet var arrowButton: UIButton!
    
    
    
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        
        
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    func mapDataOnCell(cell: Customer_PayMyBillTVCell?, indexPath: IndexPath,array : [String]) {

        //active
//        ThemeEngine.sharedInstance.attributedString(alignment: .center, font: ThemeEngine.sharedInstance.getFontNormal(size: 12), color: UIColor.black, text: "06/8/2019")
        
        //inactive
//        ThemeEngine.sharedInstance.attributedString(alignment: .center, font: ThemeEngine.sharedInstance.getFontNormal(size: 12), color: UIColor.brownGreyTwo, text: "06/8/2019")
        
        //pay
//        ThemeEngine.sharedInstance.attributedString(alignment: .center, font: ThemeEngine.sharedInstance.getFontNormal(size: 12), color: UIColor.primaryBlue, text: "06/8/2019")
        
        
        
        switch indexPath.row {
        case 0:
            print()
            
              dateLabel.attributedText = ThemeEngine.sharedInstance.attributedString(alignment: .center, font: ThemeEngine.sharedInstance.getFontNormal(size: 12), color: UIColor.black, text: "06/8/2019")
              bundleLabel.attributedText = ThemeEngine.sharedInstance.attributedString(alignment: .center, font: ThemeEngine.sharedInstance.getFontNormal(size: 12), color: UIColor.black, text: "Bundle 1")
              amountLabel.attributedText = ThemeEngine.sharedInstance.attributedString(alignment: .center, font: ThemeEngine.sharedInstance.getFontNormal(size: 12), color: UIColor.black, text: "1800 PKR")
              payNow.attributedText = ThemeEngine.sharedInstance.attributedString(alignment: .center, font: ThemeEngine.sharedInstance.getFontNormal(size: 12), color: UIColor.black, text: "Pay Now")
            
            
            
            
            
        default:
            print()
        }
        
        
        
    }
    @IBAction func arrowButtonPressed(_ sender: UIButton) {
        
        
        
    }
    
}
