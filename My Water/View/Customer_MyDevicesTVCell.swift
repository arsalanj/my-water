//
//  Customer_MyDevicesTVCell.swift
//  My Water
//
//  Created by Muhammad Mehdi on 09/05/2019.
//  Copyright © 2019 Muhammad Mehdi. All rights reserved.
//

import UIKit
import PopMenu

class Customer_MyDevicesTVCell: UITableViewCell {
    
    @IBOutlet var mainView: UIView!
    
    @IBOutlet var logoImage: UIImageView!
    @IBOutlet var imageLabel: UILabel!
    
    
    @IBOutlet var topLabel: UILabel!
    @IBOutlet var deviceLabel: UILabel!
    
    
    
    
    @IBOutlet var notActiveLabel: UILabel!
    @IBOutlet var addUserButton: UIButton!
    @IBOutlet var repairButton: UIButton!
    @IBOutlet var menuButton: UIButton!
    
    let manager = PopMenuManager.default

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        
      
        mainView.layer.borderWidth = 1
        mainView.layer.borderColor = UIColor.veryLightPinkFive.cgColor
        mainView.layer.cornerRadius = 8
        mainView.layer.shadowColor = UIColor.black2.cgColor
        mainView.layer.shadowOpacity = 1
        mainView.layer.shadowOffset = CGSize(width: 0, height: 6)
        mainView.layer.shadowRadius = 10 / 2
        mainView.layer.shadowPath = nil
        
        
        
        
        
        //popupactions
        manager.popMenuAppearance.popMenuFont = ThemeEngine.sharedInstance.getFontNormal(size: 14)
        manager.popMenuAppearance.popMenuColor.actionColor = .tint(UIColor.black)
        manager.popMenuAppearance.popMenuColor.backgroundColor = .solid(fill: .white) // A solid gray background color

    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    
    
    
    
    @IBAction func buttonAction(_ sender: UIButton) {
        print("---> function = \(#function) --> Line = \(#line) ")
        switch sender {
        case addUserButton:
            print()
        case repairButton:
            print()
        case menuButton:
            print()
            displayPopUp()
        default:
            print()

        }
        
        
    }
    
    func displayPopUp(){
        print("---> function = \(#function) --> Line = \(#line) ")

        manager.actions = [
       PopMenuDefaultAction(title: "Access", didSelect: { action in
            // action is a `PopMenuAction`, in this case it's a `PopMenuDefaultAction`
            
            // Print out: 'Action 1 is tapped'
        print("\(action.title ?? " ") is tapped")
        }),
            PopMenuDefaultAction(title: "Settings"), // Text only action
            PopMenuDefaultAction(title: "Share"), // Text only action
        ]
  
        
        manager.present(sourceView: menuButton)
        
        
      

        
        
        
        
    }
    
    
    func mapDataOnCell (cell: Customer_MyDevicesTVCell?, indexPath: IndexPath,array : [String]) {

        
        switch indexPath.row {
        case 0:
            print()
            logoImage.image = #imageLiteral(resourceName: "ic_cleanWater")
            imageLabel.attributedText = ThemeEngine.sharedInstance.attributedString(alignment: .center, font: ThemeEngine.sharedInstance.getFontNormal(size: 11), color: UIColor.primaryBlue, text: "Clean")
            topLabel.attributedText = ThemeEngine.sharedInstance.attributedString(alignment: .left, font: ThemeEngine.sharedInstance.getFontNormal(size: 10), color: UIColor.brownGreyThree, text: "")
            topLabel.isHidden = true
    
             deviceLabel.attributedText = ThemeEngine.sharedInstance.attributedString(alignment: .left, font: ThemeEngine.sharedInstance.getFontNormal(size: 16), color: UIColor.black87, text: "Office")
    
             notActiveLabel.attributedText = ThemeEngine.sharedInstance.attributedString(alignment: .center, font: ThemeEngine.sharedInstance.getFontNormal(size: 12), color: UIColor.brownGreyTwo, text: "")
            notActiveLabel.isHidden = true

            
        case 1:
            print()
            logoImage.image = #imageLiteral(resourceName: "ic_error-1")
            imageLabel.attributedText = ThemeEngine.sharedInstance.attributedString(alignment: .center, font: ThemeEngine.sharedInstance.getFontNormal(size: 11), color: UIColor.primaryBlue, text: "")
            imageLabel.isHidden = true
            topLabel.attributedText = ThemeEngine.sharedInstance.attributedString(alignment: .left, font: ThemeEngine.sharedInstance.getFontNormal(size: 10), color: UIColor.brownGreyThree, text: "Owner")
//            topLabel.isHidden = true
            
            deviceLabel.attributedText = ThemeEngine.sharedInstance.attributedString(alignment: .left, font: ThemeEngine.sharedInstance.getFontNormal(size: 16), color: UIColor.black87, text: "My Kitchen")
            
            notActiveLabel.attributedText = ThemeEngine.sharedInstance.attributedString(alignment: .center, font: ThemeEngine.sharedInstance.getFontNormal(size: 12), color: UIColor.brownGreyTwo, text: "")
            notActiveLabel.isHidden = true
            
        case 2:
            print()
            
//            self.mainView.tintColor = UIColor.brownGreyTwo
//            self.self.mainView.backgroundColor = UIColor.brownGreyTwo
            
            logoImage.image = #imageLiteral(resourceName: "ic_cleanWater")
            
            imageLabel.attributedText = ThemeEngine.sharedInstance.attributedString(alignment: .center, font: ThemeEngine.sharedInstance.getFontNormal(size: 11), color: UIColor.primaryBlue, text: "")
            imageLabel.isHidden = true
            topLabel.attributedText = ThemeEngine.sharedInstance.attributedString(alignment: .left, font: ThemeEngine.sharedInstance.getFontNormal(size: 10), color: UIColor.brownGreyThree, text: "")
                        topLabel.isHidden = true
            
            deviceLabel.attributedText = ThemeEngine.sharedInstance.attributedString(alignment: .left, font: ThemeEngine.sharedInstance.getFontNormal(size: 16), color: UIColor.black87, text: "My Work")
            
            notActiveLabel.attributedText = ThemeEngine.sharedInstance.attributedString(alignment: .center, font: ThemeEngine.sharedInstance.getFontNormal(size: 12), color: UIColor.brownGreyTwo, text: "Not active")
//            notActiveLabel.isHidden = true
            
      
        default:
            print()
        }
        
        
        
        
        
        
        
    }
    
    
}
