//
//  Extensions.swift
//  Scan & Win
//
//  Created by Muhammad Mehdi on 18/04/2019.
//  Copyright © 2019 Muhammad Mehdi. All rights reserved.
//

import Foundation
import UIKit

class ThemeEngine {
    static let sharedInstance = ThemeEngine()
    
    let smallButtonFontSize : CGFloat = 15.0

    let buttonFontSize : CGFloat = 15.0
    let labelFontSize : CGFloat = 15.0
    let expandedCellLabelFontSize : CGFloat = 12.0
    let homeScreenSubViewLabelsSize : CGFloat = 12.0
    
    
    
    
    let colorPrimary = #colorLiteral(red: 0, green: 0.6549019608, blue: 0.9019607843, alpha: 1)
    let colorPrimaryDark = UIColor(hexString: "FDC300")!
    let colorNavBar = UIColor(hexString: "745D00")!
    let colorAccent = UIColor(hexString: "facf0e")!
    let colorBtnUnPress = UIColor(hexString: "745D00")!
    let colorBtnPress = UIColor(hexString: "A27D11")!
    let colorWhite = UIColor.white
    let gradientStartColor = UIColor(hexString: "facf0e")!
    let gradientCenterColor = UIColor(hexString: "facf0e")!
    let gradientEndColor = UIColor(hexString: "facf0e")!
    let colorBtnUnPressLightGray = UIColor(hexString: "989a9d")!
    let colorText = UIColor(hexString: "745D00")!
    let colorEditTextShapeInner = UIColor(hexString: "66FFFFFF")!
    let colorProductDescription = UIColor(hexString: "668B8888")!
    let colorEditTextShapeCorner = UIColor(hexString: "40745D00")!
    let colorBlack = UIColor.black
    let colorBtnPressUnSignUp = UIColor(hexString: "FFCC00")!
    let colorLogin = UIColor(hexString: "99FFFFFF")!
    
    
    
    
    
    func attributedString(alignment : NSTextAlignment, font : UIFont, color: UIColor, text: String) -> NSAttributedString{
        
        
        
        
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.alignment = alignment
        paragraphStyle.minimumLineHeight = font.pointSize
        let attributes : [NSAttributedString.Key : Any] = [.font: font,
                          .paragraphStyle: paragraphStyle,
                          .foregroundColor: color]
        let attrString = NSAttributedString(string:text, attributes: attributes)
        
        return attrString
        
    }
    
    
    
    
    
    // Return viewCornerRadius
    func viewCornerRadius() -> CGFloat {
        return 8.0
        
    }
    //fonts
    
//    <string>GothamProMediumItalic.ttf</string>
//    <string>GothaProBla.otf</string>
//    <string>GothaProBol.otf</string>
//    <string>GothaProLig.otf</string>
//    <string>GothaProLigIta.otf</string>
//    <string>GothaProMed.otf</string>
//    <string>GothaProReg.otf</string>
    
    func getFontNormal (size: CGFloat) -> UIFont{
        guard let customFont = UIFont(name: "GothamPro", size: size) else {
            fatalError("""
        Failed to load the "lato_regular" font.
        Make sure the font file is included in the project and the font name is spelled correctly.
        """
            )
        }
        
        return customFont
        
    }
    
    
        func getFontLight (size: CGFloat)-> UIFont {
            guard let customFont = UIFont(name: "GothamPro-Light", size: size) else {
                fatalError("""
            Failed to load the "Ubuntu-Light" font.
            Make sure the font file is included in the project and the font name is spelled correctly.
            """
                )
            }
    
            return customFont
    
        }
    
    
        func getFontMedium (size: CGFloat)-> UIFont {
            guard let customFont = UIFont(name: "GothamPro-Medium", size:size) else {
                fatalError("""
            Failed to load the "Ubuntu-Medium" font.
            Make sure the font file is included in the project and the font name is spelled correctly.
            """
                )
            }
    
            return customFont
    
        }
    
    
    func getFontBold (size: CGFloat)-> UIFont {
        guard let customFont = UIFont(name: "GothamPro-Bold", size: size) else {
            fatalError("""
        Failed to load the "lato_bold" font.
        Make sure the font file is included in the project and the font name is spelled correctly.
        """
            )
        }
        
        
        
        return customFont
        
    }
    
    
    
}

extension UIColor {
    
    @nonobjc class var primaryBlue: UIColor {
        return UIColor(named: "primaryBlue")!
    }
    
    @nonobjc class var white: UIColor {
        return UIColor(named: "white")!
    }
    
    @nonobjc class var black87: UIColor {
        return UIColor(named: "black87")!
    }
    
    @nonobjc class var veryLightPink: UIColor {
        return UIColor(named: "veryLightPink")!
    }
    
    @nonobjc class var iceBlue: UIColor {
        return UIColor(named: "iceBlue")!
    }
    
    @nonobjc class var blackTwo: UIColor {
        return UIColor(named: "blackTwo")!
    }
    
    @nonobjc class var primaryBlue87: UIColor {
        return UIColor(named: "primaryBlue87")!
    }
    
    @nonobjc class var black: UIColor {
        return UIColor(named: "black")!
    }
    
    @nonobjc class var black60: UIColor {
        return UIColor(named: "black60")!
    }
    
    @nonobjc class var white50: UIColor {
        return UIColor(named: "white50")!
    }
    
    @nonobjc class var black50: UIColor {
        return UIColor(named: "black50")!
    }
    
    @nonobjc class var blackTwoTwo: UIColor {
        return UIColor(named: "blackTwoTwo")!
    }
    
    @nonobjc class var veryLightPinkTwo: UIColor {
        return UIColor(named: "veryLightPinkTwo")!
    }
    
    @nonobjc class var brownGrey: UIColor {
        return UIColor(named: "brownGrey")!
    }
    
    @nonobjc class var whiteTwo: UIColor {
        return UIColor(named: "whiteTwo")!
    }
    
    @nonobjc class var brownGreyTwo: UIColor {
        return UIColor(named: "brownGreyTwo")!
    }
    
    @nonobjc class var veryLightPinkThree: UIColor {
        return UIColor(named: "veryLightPinkThree")!
    }
    
    @nonobjc class var black4: UIColor {
        return UIColor(named: "black4")!
    }
    
    @nonobjc class var black10: UIColor {
        return UIColor(named: "black10")!
    }
    
    @nonobjc class var red: UIColor {
        return UIColor(named: "red")!
    }
    
    @nonobjc class var veryLightPinkFour: UIColor {
        return UIColor(named: "veryLightPinkFour")!
    }
    
    @nonobjc class var whiteThree: UIColor {
        return UIColor(named: "whiteThree")!
    }
    
    @nonobjc class var paleRed: UIColor {
        return UIColor(named: "paleRed")!
    }
    
    @nonobjc class var brownGreyThree: UIColor {
        return UIColor(named: "brownGreyThree")!
    }
    
    @nonobjc class var dustyOrange: UIColor {
        return UIColor(named: "dustyOrange")!
    }
    
    @nonobjc class var brownGreyTwo87: UIColor {
        return UIColor(named: "brownGreyTwo87")!
    }
    
    @nonobjc class var black2: UIColor {
        return UIColor(named: "black2")!
    }
    
    @nonobjc class var veryLightPinkFive: UIColor {
        return UIColor(named: "veryLightPinkFive")!
    }
    
}

// Text styles

extension UIFont {
    
    class var titleSecondaryButton: UIFont {
        return UIFont(name: "GothamPro-Medium", size: 15.0)!
    }
    
    class var linkMain: UIFont {
        return UIFont(name: "GothamPro-Medium", size: 14.0)!
    }
    
    class var titleMainButton: UIFont {
        return UIFont(name: "GothamPro-Medium", size: 15.0)!
    }
    
    class var h1: UIFont {
        return UIFont(name: "GothamPro-Medium", size: 18.0)!
    }
    
    class var titleInput: UIFont {
        return UIFont(name: "GothamPro", size: 13.0)!
    }
    
    class var linkSecondary: UIFont {
        return UIFont(name: "GothamPro-Medium", size: 12.0)!
    }
    
    class var titleInputPlaceholder: UIFont {
        return UIFont(name: "GothamPro", size: 14.0)!
    }
    
    class var titleCodePhoneNumber: UIFont {
        return UIFont(name: "GothamPro", size: 14.0)!
    }
    
    class var titleAlert: UIFont {
        return UIFont(name: "GothamPro-Medium", size: 13.0)!
    }
    
    class var titleInputAlert: UIFont {
        return UIFont(name: "GothamPro", size: 12.0)!
    }
    
    class var h2: UIFont {
        return UIFont(name: "GothamPro-Medium", size: 16.0)!
    }
    
    class var titleMenu: UIFont {
        return UIFont(name: "GothamPro-Medium", size: 13.0)!
    }
    
    class var titleSplash: UIFont {
        return UIFont(name: "GothamPro-Medium", size: 14.0)!
    }
    
    class var h3: UIFont {
        return UIFont(name: "GothamPro-Medium", size: 14.0)!
    }
    
    class var inputTextStyle: UIFont {
        return UIFont(name: "GothamPro", size: 14.0)!
    }
    
    class var linkCancel: UIFont {
        return UIFont(name: "GothamPro-Medium", size: 14.0)!
    }
    
    class var linkBack: UIFont {
        return UIFont(name: "GothamPro-Medium", size: 14.0)!
    }
    
    class var linkReject: UIFont {
        return UIFont(name: "GothamPro-Medium", size: 14.0)!
    }
    
    class var paragraph: UIFont {
        return UIFont(name: "GothamPro", size: 13.0)!
    }
    
    class var tableTitle: UIFont {
        return UIFont(name: "GothamPro-Medium", size: 11.0)!
    }
    
    class var tableContent: UIFont {
        return UIFont(name: "GothamPro", size: 12.0)!
    }
    
    class var titleDahboard: UIFont {
        return UIFont(name: "GothamPro-Medium", size: 12.0)!
    }
    
    class var titleDeviceDetails: UIFont {
        return UIFont(name: "GothamPro-Medium", size: 12.0)!
    }
    
    class var titleNoResults: UIFont {
        return UIFont(name: "GothamPro", size: 12.0)!
    }
    
    class var titleStatus: UIFont {
        return UIFont(name: "GothamPro", size: 10.0)!
    }
    
    class var h5: UIFont {
        return UIFont(name: "GothamPro-Medium", size: 10.0)!
    }
    
}
