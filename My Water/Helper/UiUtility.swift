//
//  Extensions.swift
//  Scan & Win
//
//  Created by Muhammad Mehdi on 18/04/2019.
//  Copyright © 2019 Muhammad Mehdi. All rights reserved.
//

import Foundation
import UIKit
import SwiftOverlays
import NVActivityIndicatorView
import MaterialComponents

class UIUtility {
    static let sharedInstance = UIUtility()
    
    
    
    
    /**
     * Show iOS default alert
     * @params: title:String, message:String, btnTitle:String
     * @return: void
     */
    //    func showAlert(title:String, message:String, btnTitle:String){
    //        let alert = UIAlertView()
    //        alert.title = title
    //        alert.message = message
    //        alert.addButton(withTitle: btnTitle)
    //        alert.show()
    //    }
    
    func showAlert(title:String, message:String, btnTitle:String){
        //        let alert = UIAlertView()
        //        alert.title = title
        //        alert.message = message
        //        alert.addButton(withTitle: btnTitle)
        //        alert.show()
        
        
        
        let alertController = MDCAlertController(title: title, message: message)
        let action = MDCAlertAction(title:btnTitle) { (action) in print("OK")
            
            alertController.dismiss(animated: true, completion: nil)
            
        }
        alertController.addAction(action)
        //        present(alertController, animated:true, completion:...)
        
        UIApplication.shared.keyWindow?.rootViewController?.present(alertController, animated: true, completion: nil)
        
        
        
    }
    
    
    
    func showAlertWithCompletion(title:String, message:String, vc : UIViewController, completion: @escaping (Bool) -> Void){
        
        
        //        let myString  = "Alert Title"
        var myTitleString = NSMutableAttributedString()
        myTitleString = NSMutableAttributedString(string: title as String, attributes: [NSAttributedString.Key.font:ThemeEngine.sharedInstance.getFontBold(size: ThemeEngine.sharedInstance.buttonFontSize)])
        myTitleString.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.black, range: NSRange(location:0,length:title.count))
        
        var myMessageString = NSMutableAttributedString()
        myMessageString = NSMutableAttributedString(string: message as String, attributes: [NSAttributedString.Key.font:ThemeEngine.sharedInstance.getFontNormal(size: ThemeEngine.sharedInstance.smallButtonFontSize)])
        myMessageString.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.black, range: NSRange(location:0,length:message.count))
        
        
        
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
        //
        alert.setValue(myTitleString, forKey: "attributedTitle")
        alert.setValue(myMessageString, forKey: "attributedMessage")
        //        //for background view
        
        let margin:CGFloat = 10.0
        let rect = CGRect(x: 0, y: 0, width: alert.view.bounds.size.width , height: alert.view.bounds.height)
        var customView = UIView(frame: rect)
        //        customView = UIUtility.sharedInstance.createGradientView(view: customView)
        //        alert.view.addSubview(customView)
        alert.view.backgroundColor = customView.backgroundColor
        
        
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
            switch action.style{
            case .default:
                completion(true)
            case .cancel:
                completion(false)
                
            case .destructive:
                print("destructive")
                
                
            }}))
        
        
        alert.addAction(UIAlertAction(title: "Cancel", style: .default, handler: { action in
            switch action.style{
            case .default:
                completion(false)
            case .cancel:
                completion(false)
                
            case .destructive:
                print("destructive")
                
                
            }}))
        
        
        vc.present(alert, animated: true, completion: nil)
        
        
        
    }
    
    
    /*
     func showAlertWithCompletion(title:String, message:String, vc : UIViewController, completion: @escaping (Bool) -> Void){
     
     let appearance = SCLAlertView.SCLAppearance(
     kTitleFont: ThemeEngine.sharedInstance.getFontBold(size: ThemeEngine.sharedInstance.buttonFontSize),
     kTextFont: ThemeEngine.sharedInstance.getFontNormal(size: ThemeEngine.sharedInstance.smallButtonFontSize),
     kButtonFont: ThemeEngine.sharedInstance.getFontMedium(size: ThemeEngine.sharedInstance.smallButtonFontSize),
     showCloseButton: false,
     showCircularIcon: false, dynamicAnimatorActive: false
     )
     
     
     
     let alert = SCLAlertView(appearance: appearance)
     
     alert.contentView.insertSubview(createGradientView(view: alert.contentView), at: 0)
     alert.addButton("Ok") {
     completion(true)
     }
     alert.addButton("Cancel") {
     completion(false)
     }
     
     alert.showError(title, subTitle: message)
     
     
     }
     */
    func showAlertWithDuration(title:String, message:String, btnTitle:String, time: Double ){
        let alert = UIAlertView()
        alert.title = title
        alert.message = message
        alert.addButton(withTitle: btnTitle)
        alert.show()
        
        // change to desired number of seconds (in this case 5 seconds)
        let when = DispatchTime.now() + time
        DispatchQueue.main.asyncAfter(deadline: when){
            // your code with delay
            alert.dismiss(withClickedButtonIndex: 0, animated: true)
        }
    }
    
    
    
    
    
    
    /**
     * Start activity indicator / Stop and remove from view
     */
    func startRemoveActivityIndicator(start:Bool, aIndicator:UIActivityIndicatorView){
        if start {
            aIndicator.startAnimating()
        } else {
            aIndicator.stopAnimating()
            aIndicator.removeFromSuperview()
        }
    }
    
    /**
     * Check email address is valid
     * @param: emailAdd string
     *  @return: Bool
     */
    func isValidEmail(emailAdd:String) -> Bool {
        // println("validate calendar: \(testStr)")
        let emailRegEx = "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: emailAdd)
    }
    //  /*
    /***
     * Remove tab(\t) newline (\n) and spaces from string and return
     */
    func removeNewLineTabAndSpaces(myString:String) -> String {
        let trimmedString = myString.trimmingCharacters(in: .whitespacesAndNewlines)//myString.stringByTrimmingCharactersInSet(
        //            NSCharacterSet.whitespaceAndNewlineCharacterSet()
        //        )
        return trimmedString
    }
    
    /***
     * When unable to load API or found any error from api side then show error screen
     */
    //    func showNoInternetScreen(message:String, selDelegate:NoInternetViewControllerDelegate, selfObj:AnyObject){
    //        let vc = NoInternetViewController(nibName: "NoInternetViewController", bundle: nil)
    //        vc.delegate = selDelegate
    //        vc.msg = message
    //        let navigationController: UINavigationController = UINavigationController(rootViewController: vc)
    //        selfObj.present(navigationController, animated: true, completion: nil)
    ////        selfObj.present(navigationController, animated: true, completion: nil)
    //    }
    
    /**
     * This function will return product price string with comma. Eg. 75,000
     */
    func getPriceWithComma( pPrice:Double) -> String {
        var priceString = ""
        let numberFormatter = NumberFormatter()
        numberFormatter.numberStyle = NumberFormatter.Style.decimal//NumberFormatter.Style.DecimalStyle
        priceString = numberFormatter.string(from: pPrice as NSNumber)!//numberFormatter.stringFromNumber(NSNumber(pPrice))//(NSNumber(pPrice))!
        return priceString
    }
    
    
    /**
     * Get configurable product price with user selected options
     */
    //    func getProductPriceWithSelectedOptions( product:Product) -> Double {
    //        var pPrice = product.price
    //
    //        for availableIn in product.availableIn {
    //            for aiOption in availableIn.options {
    //                if aiOption.selected  {
    //                    pPrice += aiOption.pricing_value
    //                }
    //            }
    //        }
    //
    //        return pPrice
    //    }
    
    //<<<<<<< HEAD
    
    
    
    func roundAnImage(imageView : UIImageView) -> UIImageView {
        imageView.layer.cornerRadius = imageView.frame.size.width / 2;
        imageView.clipsToBounds = true;
        
        return imageView
        
    }
    
    //=======
    func addDoneButton(view : UIView, textField: UITextField) {
        let keyboardToolbar = UIToolbar()
        keyboardToolbar.sizeToFit()
        let flexBarButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace,
                                            target: nil, action: nil)
        let doneBarButton = UIBarButtonItem(barButtonSystemItem: .done,
                                            target: view, action: #selector(UIView.endEditing(_:)) )
        doneBarButton.tintColor = UIColor.black
        keyboardToolbar.items = [flexBarButton, doneBarButton]
        textField.inputAccessoryView = keyboardToolbar
        
        
        
    }
    
    
    func addDoneButtontoSearchBar(view : UIView, searchbar: UISearchBar) {
        let keyboardToolbar = UIToolbar()
        keyboardToolbar.sizeToFit()
        let flexBarButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace,
                                            target: nil, action: nil)
        let doneBarButton = UIBarButtonItem(barButtonSystemItem: .done,
                                            target: view, action: #selector(UIView.endEditing(_:)) )
        keyboardToolbar.items = [flexBarButton, doneBarButton]
        searchbar.inputAccessoryView = keyboardToolbar
        
        
        
    }
    
    
    
    
    
    
    func setLoadingOverlay (view: UIView,type: NVActivityIndicatorType ,  color: UIColor) -> UIView {
        
        let activityIndicator = NVActivityIndicatorView(frame: view.frame, color: color)
        activityIndicator.type = type
        //        doctorImage.addSubview(imageActivityIndicator)
        
        
        return activityIndicator
        
    }
    
    
    func verifyUrl(urlString: String?) -> Bool {
        guard let urlString = urlString,
            let url = URL(string: urlString) else {
                print("\(#function ) returning false")
                return false
        }
        
        return UIApplication.shared.canOpenURL(url)
    }
    
    
    
    
    func roundImageWithBorder (imageView : UIImageView, color: UIColor, borderWidth: CGFloat) -> UIImageView {
        imageView.layer.cornerRadius = imageView.frame.size.width / 2;
        imageView.clipsToBounds = true;
        imageView.layer.masksToBounds=true
        imageView.layer.borderWidth = borderWidth
        imageView.layer.borderColor = color.cgColor
        //        imageView.layer.cornerRadius=imageView.bounds.width/2
        return imageView
        
    }
    
    
    
    
    func imageOrientation(_ src:UIImage)->UIImage {
        if src.imageOrientation == UIImage.Orientation.up {
            return src
        }
        var transform: CGAffineTransform = CGAffineTransform.identity
        switch src.imageOrientation {
        case UIImageOrientation.down, UIImageOrientation.downMirrored:
            transform = transform.translatedBy(x: src.size.width, y: src.size.height)
            transform = transform.rotated(by: CGFloat(M_PI))
            break
        case UIImageOrientation.left, UIImageOrientation.leftMirrored:
            transform = transform.translatedBy(x: src.size.width, y: 0)
            transform = transform.rotated(by: CGFloat(M_PI_2))
            break
        case UIImageOrientation.right, UIImageOrientation.rightMirrored:
            transform = transform.translatedBy(x: 0, y: src.size.height)
            transform = transform.rotated(by: CGFloat(-M_PI_2))
            break
        case UIImageOrientation.up, UIImageOrientation.upMirrored:
            break
        }
        
        switch src.imageOrientation {
        case UIImageOrientation.upMirrored, UIImageOrientation.downMirrored:
            transform.translatedBy(x: src.size.width, y: 0)
            transform.scaledBy(x: -1, y: 1)
            break
        case UIImageOrientation.leftMirrored, UIImageOrientation.rightMirrored:
            transform.translatedBy(x: src.size.height, y: 0)
            transform.scaledBy(x: -1, y: 1)
        case UIImageOrientation.up, UIImageOrientation.down, UIImageOrientation.left, UIImageOrientation.right:
            break
        }
        
        let ctx:CGContext = CGContext(data: nil, width: Int(src.size.width), height: Int(src.size.height), bitsPerComponent: (src.cgImage)!.bitsPerComponent, bytesPerRow: 0, space: (src.cgImage)!.colorSpace!, bitmapInfo: CGImageAlphaInfo.premultipliedLast.rawValue)!
        
        ctx.concatenate(transform)
        
        switch src.imageOrientation {
        case UIImageOrientation.left, UIImageOrientation.leftMirrored, UIImageOrientation.right, UIImageOrientation.rightMirrored:
            ctx.draw(src.cgImage!, in: CGRect(x: 0, y: 0, width: src.size.height, height: src.size.width))
            break
        default:
            ctx.draw(src.cgImage!, in: CGRect(x: 0, y: 0, width: src.size.width, height: src.size.height))
            break
        }
        
        let cgimg:CGImage = ctx.makeImage()!
        let img:UIImage = UIImage(cgImage: cgimg)
        
        return img
    }
    
    
    
    
    
    
    
    
    
    //swift overlays
    
    func showProgressOverlayBlockingWithText(text: String){
        
        SwiftOverlays.showBlockingWaitOverlayWithText(text)
        
        print("---> Line = \(#line) in function = \(#function)")
        
    }
    
    func removeProgressOverlayBlockingWithText() {
        
        SwiftOverlays.removeAllBlockingOverlays()
        
        
    }
    
    
    
    
    func instantiateVC(storyboard : String, identifier: String) -> UIViewController{
        let storyboard = UIStoryboard(name: storyboard, bundle: nil)
        
        let viewController = storyboard.instantiateViewController(withIdentifier: identifier)
        
        return viewController
    }
    
    func instantiateNavController(storyboard : String, identifier: String) {
        let storyboard = UIStoryboard(name: storyboard, bundle: nil)
        
        let viewController = storyboard.instantiateViewController(withIdentifier: identifier) as! UINavigationController
        
        //        SELF.present(viewController, animated: true, completion: nil)
        
        AppDelegate.shared.window?.rootViewController = viewController
        AppDelegate.shared.window?.makeKeyAndVisible()
        
        
        
        //        SELF.dismiss(animated: true) { () -> Void in
        //            //Perform segue or push some view with your code
        //            UIApplication.shared.keyWindow?.rootViewController = viewController
        //        }
        //
        //        return viewController
    }
    
    
    
    //MARK:- Toast
    func showToast(controller: UIViewController, message : String, seconds: Double) {
        let alert = UIAlertController(title: nil, message: message, preferredStyle: .alert)
        alert.view.backgroundColor = UIColor.black
        alert.view.alpha = 0.6
        alert.view.layer.cornerRadius = 15
        
        controller.present(alert, animated: true)
        
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + seconds) {
            alert.dismiss(animated: true)
        }
    }
    
    
    //MARK:- for displaying error on textfields
    
    func setError(error: Bool,message: String, textFieldController : MDCTextInputControllerOutlined, textFieldErrorLblArray: [UILabel], textFieldControllerArray: [MDCTextInputControllerOutlined]) {
        
        
        
        
        
        for (index,controller) in (textFieldControllerArray.enumerated()){
            
            
            
            
            if controller == textFieldController {
                
                if error{
                    
                    controller.setErrorText(" ", errorAccessibilityValue: nil)
                    textFieldErrorLblArray[index].isHidden = false
                    //                    textFieldErrorLblArray?[index].text = message
                    textFieldErrorLblArray[index].attributedText = ThemeEngine.sharedInstance.attributedString(alignment: .left, font: ThemeEngine.sharedInstance.getFontNormal(size: 12), color: UIColor.red, text: message)
                    
                }else{
                    
                    controller.setErrorText(nil, errorAccessibilityValue: nil)
                    textFieldErrorLblArray[index].isHidden = true
                    textFieldErrorLblArray[index].attributedText = ThemeEngine.sharedInstance.attributedString(alignment: .left, font: ThemeEngine.sharedInstance.getFontNormal(size: 12), color: UIColor.red, text: "")
                    
                }
                
                
                
            }
            
            
        }
        
        
        
        
    }
    
    
    
}
